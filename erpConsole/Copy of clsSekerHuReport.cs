using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Reflection;
using System.Data;

namespace giyusConsole
{
    class clsSekerHuReport
    {
        public enum color {yellow,gray,blue};
        public Excel.Workbook oWorkBook;
        public Excel.ApplicationClass oAppClass = null;
        public Excel.Worksheet oWorkSheet;
        public Excel.Worksheet oWorkSheet2; //�� ����� ���� ������

        public Excel.Range workingRangeCells = null;
        public Excel.Range A1MarginRange = null;

        public static string[] arrDelimiter = new string[1] { "!$!" };
        public string[] strMarginSplitter = new string[1] { ";" }; // between margins
        public string[] strRangeSplitter = new string[1] { "!" }; // between each range name and cells
        public string[] yaelSplitter = new string[1] { "!y@" };

        //static string sSource = "loadPsyc";
        //static string sLog = "loadPsyc";
        //static string strConStr;
        static string strData;
        //static string strFields;
        //static string strValues;
        static public string strWordInputDir = "";
        //static string[] arrfields;
        //static string[] arrData;
        static string[] strDataSplitter = new string[1];
        static string[] strDataSplitter2 = new string[1];
        //static int lineCount = 0;
        //static string strFileLineNo = "0";
        //static int intNumOfErrors = 0;

        static public string emp_num=""; //using it for save
        static public string strConvDate = ""; //but we getting emp_id from excel file
        static public string emp_id = ""; //but we getting emp_id from excel file
        //static public string strPointsRaised="";
        //static public string strCanResponse = "";
        //static public string strRemarks = "";
        //static public string strPsycName = "";
        string[] arrMarginsRanges = null;
        int mySum = 0;
        int startLine = 9;
        int currLine = 0;
        string strColName = "";
        Boolean blnHuOrManagerFound = false;
        int idxRptCol_forExcel = 0;
        int idxRptColForJobCounter_Excel = 8; //start of jobs
        int jobLines_13 = 0;
        DataTable dtSeker = null;
        int intExcelLineCount = 8;
        Boolean blnWordVisible = Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);


        public void doReport(string prmShtachDelimeted)
        {
            try
            {
                string msg = "����� ��� ��� ���� ����� ����....";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg, 2,7,true); //msgType - 2=info , 1=error

                strWordInputDir = Program.paramStrWordWorkDir;//ConfigurationManager.AppSettings["wordInputDir"];
                //            strDataSplitter[0] = ",";
                //            strDataSplitter2[0] = "!$!";
                //strConStr = ConfigurationManager.AppSettings["rafManPowerConnectionString"];
                // *******************  make connection ******************************************************
                RafDB.clsDbGlobal.makeConn();
                    
                //string[] arrSubData = null;
                //string[] arrSubMarginsRanges;

                CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                oAppClass = new Excel.ApplicationClass();

                //******************************excel Alerts ****************************
                oAppClass.Visible = blnWordVisible;
                oAppClass.DisplayAlerts = blnWordVisible;

                #region excel
                oWorkBook = oAppClass.Workbooks.Open(strWordInputDir + "\\sekerHeadUnitReport.xlsx",
                    Missing.Value, (object)true, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                //******************************excel Alerts ****************************
                // ******************************************************************//
                //get display_Names sheet
                oWorkSheet = FindExcelWorksheetByName(oWorkBook, "��� ���� �����");
                if (oWorkSheet == null)
                {
                    msg = @"������ ��� - """ + "��� ���� �����" + @""" �� ����";
                    Program.writeToTasksLogTableForRow(msg, 1,7);
                    throw new Exception(msg);
                }

                oWorkSheet2 = FindExcelWorksheetByName(oWorkBook, "�� ����� ��� �����");
                if (oWorkSheet2 == null)
                {
                    msg = @"������ ��� - """ + "�� ����� ��� �����" + @""" �� ����";
                    Program.writeToTasksLogTableForRow(msg, 1, 7);
                    throw new Exception(msg);
                }

                Program.lineCount = 8;
                dtSeker = getSekerForHuReport(prmShtachDelimeted);

                if (dtSeker == null)
                {
                    msg = "�� ����� ����� ��� ����� �������, ��� ���� ��� : \n";
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTableForRow(msg, 1,7);
                    return;
                }
                currLine = startLine;
                for (int idxRpt = 0; idxRpt < dtSeker.Rows.Count; idxRpt++)
                {
                    //************************************************ treat jobs *********************************************************
                    //jobLines_13 = idxRpt % 13; // get the modulo of row %12 and add this to the columns count of jobs in the excel
                    //treat jobs - column - 5+2 and HU : 5 + 2 + 13
                    //+2 between the dataTable and the excel,+ add row num for next column (jobLine_13)
                    idxRptCol_forExcel = idxRptColForJobCounter_Excel;
                    strColName = getCellName(currLine, idxRptCol_forExcel);
                        //sekerSelections.sekerSelections_JobSelection, --6
                        //any job selection is in a different row,the column of job selections is in col 6,we have 13 each
                        //we order the job selections by res_role_num, to keep correct order, see the storedProcedure
                    oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["sekerSelections_JobSelection"].ToString();//always col 6 in query
                    //+2 between the dataTable and the excel, + 13 distance of headUnit jobs + add row num for next column (jobLine_13)
                    idxRptCol_forExcel = idxRptColForJobCounter_Excel +13;
                    strColName = getCellName(currLine, idxRptCol_forExcel);
                        //sekerSelections.sekerSelections_HeadUnitJobSelection, --7
                        //any job selection is in a different row,the column of head job selections is in col 7,we have 13 each
                        //we order the job selections by res_role_num, to keep correct order, see the storedProcedure
                    oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["sekerSelections_HeadUnitJobSelection"].ToString();//always col 7 in query
                    //************************************************ treat jobs ********************************************************
                    idxRptColForJobCounter_Excel++; //next job column
                    // 0 to 12 jobs, we do this only for the first row of every 13
                    if ((idxRpt+1) % 13 == 0)
                    {
                        //when getting to the end of 13 jobs, do the mesureMeants and other fields.
                        doFirstWorkerRowForReport(idxRpt, Convert.ToDateTime(dtSeker.Rows[idxRpt]["seker_date"]).ToShortDateString());
                    }
                    if ((idxRpt+1) % 13 == 0 && idxRpt > 0) // >0 to avoid the first one
                    {
                        currLine++; //get next excell row
                        idxRptColForJobCounter_Excel = 8;
                    }
                }
                msg = "����� ��� ���� ������ ������� ������";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg, 2,7,true);

                makeWorkingPage(startLine,currLine);

                #endregion excel

                #region close all excel
                try
                {
                    // ******************  close the excell documents *******************
                    object objSave = false;
                    object objTrue = true;
                    if (oWorkBook != null)
                    {
                        string strDateTime = Guid.NewGuid().ToString();

                        Program.StrPptDownFileResult = "sekerHuReport_" + strDateTime + ".xlsx";
                        oWorkBook.SaveAs(((object)(Program.paramStrOutputDir + "\\reports\\" +
                                    Program.StrPptDownFileResult)),
                                    Missing.Value, Missing.Value, Missing.Value, (object)false, Missing.Value,
                                    Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, Missing.Value,
                                    (object)false, Missing.Value, Missing.Value,Missing.Value);
                        //close the first workBook
                        //oWorkBook.Close(objSave, (object)strFullFileName, Missing.Value);
                        oWorkBook.Close(objSave, Missing.Value, objTrue);
                        NAR(oWorkBook);
                        oWorkBook = null;
                        Process[] prc = Process.GetProcesses();
                        for (int idx = 0; idx < prc.Length; idx++)
                        {
                            try
                            {
                                if (prc[idx].MainModule.ModuleName.StartsWith("EXCEL"))
                                {
                                    prc[idx].Kill();
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    // ******************  close the excell documents *******************
                }
                catch (Exception ex)
                {
                    msg = "����� ����� : \n"
                                    + " - ����� = " + ex.Message
                                    + "\n - ������ = " + ex.StackTrace;
                    string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message;
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTableForRow(msg2, 1,7);

                }
                #endregion close all excel
            }
            catch (Exception ex)
            {
                string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message
                                + "\n - ������ = " + ex.StackTrace;
                string msg3 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg2, true);
                Program.writeToTasksLogTableForRow(msg3, 1,7);

            }
            finally
            {
            }
        }


        private void doFirstWorkerRowForReport(int idxRpt, string prmSekerDate)
        {
            intExcelLineCount++;
            //**********************************************************************************
            #region first row of worker
            blnHuOrManagerFound = false; //reset every row of excel
            int intStartCol = 1;
            //id
            strColName = getCellName(currLine, intStartCol);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = intExcelLineCount-8;
            //id next page
            strColName = getCellName(currLine, intStartCol + 40);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = intExcelLineCount-8;
            //date
            strColName = getCellName(currLine, intStartCol+1);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = Convert.ToDateTime(dtSeker.Rows[idxRpt]["seker_date"]).ToShortDateString();
            //isSekerDone - yes/no
            strColName = getCellName(currLine, intStartCol+2);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = rafBL.clsUtil.convertBooleanToYesOrNo(dtSeker.Rows[idxRpt]["seker_isSekerDone"]);
            //emp_id
            strColName = getCellName(currLine, intStartCol + 3);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["emp_id"].ToString();
            //name
            strColName = getCellName(currLine, intStartCol + 4);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["name"].ToString();
            //name
            strColName = getCellName(currLine, intStartCol + 41);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["name"].ToString();

            //treat shetach - start
            strColName = getCellName(currLine, intStartCol + 6);
            if (dtSeker.Rows[idxRpt]["SHETACH_desc"] != DBNull.Value)
            {
                int intFindastrix = dtSeker.Rows[idxRpt]["SHETACH_desc"].ToString().IndexOf("*");
                if (intFindastrix > -1)
                {
                    oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 =
                                    dtSeker.Rows[idxRpt]["SHETACH_desc"].ToString().Substring(intFindastrix + 3);
                }
                else
                {
                    oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 =
                        dtSeker.Rows[idxRpt]["SHETACH_desc"].ToString();
                }
            }
            //treat shetach - end

            //sum selections
            strColName = getCellName(currLine, intStartCol + 5);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt]["seker_sumSelection"].ToString();

            //treat measures - start
            int intStart = 8;
            idxRptCol_forExcel = intStart + 25; //+26 between the dataTable and the excel
            for (int idxRptCol = intStart; idxRptCol < dtSeker.Columns.Count - 1; idxRptCol++)
            {
                idxRptCol_forExcel++;
                if (idxRptCol_forExcel == intStartCol + 40 || idxRptCol_forExcel == intStartCol + 41)
                {
                    //if in pos of seconed auto_num or name, goto next columns
                    idxRptCol_forExcel += 2;
                }
                strColName = getCellName(currLine, idxRptCol_forExcel);
                //convert true and false to 1 or empty
                if (idxRptCol_forExcel == intStartCol + 42 || idxRptCol_forExcel == intStartCol + 43)
                {
                    if (Convert.ToBoolean(dtSeker.Rows[idxRpt][idxRptCol]))
                    {
                        oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = "1";
                        blnHuOrManagerFound = true;
                    }
                    else
                    {
                        oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = "";
                    }
                }
                else
                {
                    oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = dtSeker.Rows[idxRpt][idxRptCol].ToString();
                }

            }
            //if found manager or HU that voted then color the employee name
            if (blnHuOrManagerFound)
            {
                //decide colors
                //if manager or HU and 5 or above amit/kafif
                //17,18 amit(seker_SourcesMyColleagues) + kafif(seker_SourcesMyWorkers)      
                if (Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyWorkers"]) + Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyColleagues"]) >= 5)
                {
                    putColor(currLine, intStartCol + 4, color.yellow);
                    putColor(currLine, intStartCol + 41, color.yellow);
                }
                else if (Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyWorkers"]) + Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyColleagues"]) <= 5
                         && Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyWorkers"]) + Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyColleagues"]) > 0)
                {
                    putColor(currLine, intStartCol + 4, color.gray);
                    putColor(currLine, intStartCol + 41, color.gray);
                }
                else if (Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyWorkers"]) + Convert.ToInt32(dtSeker.Rows[idxRpt]["seker_SourcesMyColleagues"]) == 0)
                {
                    putColor(currLine, intStartCol + 4, color.blue);
                    putColor(currLine, intStartCol + 41, color.blue);
                }
            }
            //get seker more data from data table, and put more values
            DataTable dtSekerMoreData = getSekerMoreDate(dtSeker.Rows[idxRpt]["emp_num"].ToString(), prmSekerDate);
            if (dtSekerMoreData != null && dtSekerMoreData.Rows.Count > 0)
            {
                strColName = getCellName(currLine, 51);
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = rafBL.clsUtil.convertNullToEmpty(dtSekerMoreData.Rows[0]["POSITION_desc"]);

                strColName = getCellName(currLine, 52);
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = rafBL.clsUtil.convertNullToEmpty(dtSekerMoreData.Rows[0]["years"]);

                strColName = getCellName(currLine, 53);
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Value2 = rafBL.clsUtil.convertNullToEmpty(dtSekerMoreData.Rows[0]["ROLE_LEVEL_desc"]);
            }
            #endregion first row of worker
        }




        private void makeWorkingPage(int startLine, int endLine)
        {
            string msg = "����� �� ����� ���� ����� ����....";
            Program.writeToScreenAndToLog(msg, false);
            Program.writeToTasksLogTableForRow(msg, 2, 7,true); //msgType - 2=info , 1=error

            //get all data range from HuReport and copy to the workingPage

            A1MarginRange = oWorkSheet.get_Range("A" + startLine.ToString() + ":" +
                                                 "E" + endLine.ToString(), Type.Missing);

            workingRangeCells = oWorkSheet2.get_Range("A" + (startLine).ToString() + ":" +
                                                      "E" + (endLine).ToString(), Type.Missing);
            A1MarginRange.Copy(workingRangeCells);

            msg = "����� �� ����� ���� ������ ������� ������....";
            Program.writeToScreenAndToLog(msg, false);
            Program.writeToTasksLogTableForRow(msg, 2, 7,true); //msgType - 2=info , 1=error
        }

        private void putColor(int row, int col, Enum myColor)
        {
            DataTable dtParams = RafDB.clsDbGlobal.getParamsByTableNum(0, 25);
            object ColorYellow = dtParams.Rows[0]["param_value"].ToString();
            object ColorGray = dtParams.Rows[1]["param_value"].ToString();
            object ColorBlue = dtParams.Rows[2]["param_value"].ToString();

            string strColName = getCellName(row, col);
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.Pattern = Excel.XlPattern.xlPatternSolid;
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.PatternColorIndex = Excel.XlPattern.xlPatternAutomatic;
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.TintAndShade = 0;
            oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.PatternTintAndShade = 0;
            if (Convert.ToInt32(myColor) == Convert.ToInt32(color.yellow))
            {
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.Color = ColorYellow;
            }
            else if (Convert.ToInt32(myColor) == Convert.ToInt32(color.gray))
            {
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.Color = ColorGray;
            }
            else if (Convert.ToInt32(myColor) == Convert.ToInt32(color.blue))
            {
                oWorkSheet.get_Range(strColName + ":" + strColName, Type.Missing).Interior.Color = ColorBlue;
            }
        }


        private DataTable getSekerMoreDate(string empNum, string sekerDate)
        {
            string strInParamsNames = "EmployeeNum,Date";
            string strInParamsValues = empNum + "," + sekerDate;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return RafDB.clsDbGlobal.GetDtFromSPNoReturnValue("GetEmployeeSeker_Position_RoleLevel_Duration", arrInParamsNames, arrInParamsValues);
        }

        private DataTable getSekerForHuReport(string sheatchDelimeted)
        {
            string strInParamsNames = "sheatchDelimeted";
            string strInParamsValues = sheatchDelimeted;
            string[] arrInParamsNames = strInParamsNames.Split('@');
            string[] arrInParamsValues = strInParamsValues.Split('@');
            return RafDB.clsDbGlobal.GetDtFromSPNoReturnValue("getSekerForHuReport", arrInParamsNames, arrInParamsValues);
        }

        //private void save()
        //{
        //    string strInParamsNames = "seker_emp" + arrDelimiter[0] +
        //            "seker_date" + arrDelimiter[0] +
        //            "seker_sumSelection" + arrDelimiter[0] +
        //            "seker_JobSelection1" + arrDelimiter[0] +
        //            "seker_JobSelection2" + arrDelimiter[0] +
        //            "seker_JobSelection3" + arrDelimiter[0] +
        //            "seker_JobSelection4" + arrDelimiter[0] +
        //            "seker_JobSelection5" + arrDelimiter[0] +
        //            "seker_JobSelection6" + arrDelimiter[0] +
        //            "seker_JobSelection7" + arrDelimiter[0] +
        //            "seker_JobSelection8" + arrDelimiter[0] +
        //            "seker_JobSelection9" + arrDelimiter[0] +
        //            "seker_JobSelection10" + arrDelimiter[0] +
        //            "seker_JobSelection11" + arrDelimiter[0] +
        //            "seker_JobSelection12" + arrDelimiter[0] +
        //            "seker_JobSelection13" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection1" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection2" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection3" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection4" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection5" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection6" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection7" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection8" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection9" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection10" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection11" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection12" + arrDelimiter[0] +
        //            "seker_HeadUnitJobSelection13" + arrDelimiter[0] +
        //            "seker_measureProf" + arrDelimiter[0] +
        //            "seker_measureLeadership" + arrDelimiter[0] +
        //            "seker_measureMissionManagment" + arrDelimiter[0] +
        //            "seker_measureTeamWork" + arrDelimiter[0] +
        //            "seker_measureSystemicWatching" + arrDelimiter[0] +
        //            "seker_measureUnderstandBusiness" + arrDelimiter[0] +
        //            "seker_measureAverage" + arrDelimiter[0] +
        //            "seker_SourcesMyHeadUnit" + arrDelimiter[0] +
        //            "seker_SourcesMyManager" + arrDelimiter[0] +
        //            "seker_SourcesMyColleagues" + arrDelimiter[0] +
        //            "seker_SourcesMyWorkers" + arrDelimiter[0] +
        //            "seker_SourcesFromOrg" + arrDelimiter[0] +
        //            "seker_SourcesOutOfOrg" + arrDelimiter[0] +
        //            "seker_PowerPoints" + arrDelimiter[0] +
        //            "seker_WeakPoints";
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(emp_num + arrDelimiter[0]);
        //    sb.Append(strConvDate + arrDelimiter[0]);
        //    sb.Append(mySum.ToString() + arrDelimiter[0]);
        //    for (int idxArr = 5; idxArr < arrMarginsRanges.Length; idxArr++)
        //    {
        //        if (idxArr == 37) //average measurments
        //        {
        //            arrMarginsRanges[idxArr] =
        //                (
        //                    (
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 1]) +
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 2]) +
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 3]) +
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 4]) +
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 5]) +
        //                    Convert.ToInt32(arrMarginsRanges[idxArr - 6])
        //                    )
        //                / 6).ToString();
        //        }
        //        sb.Append(arrMarginsRanges[idxArr]);
        //        if (idxArr < arrMarginsRanges.Length-1)
        //        {
        //            sb.Append(arrDelimiter[0]);
        //        }
        //    }
        //    string strInParamsValues = sb.ToString();
        //    string[] arrInParamsNames = strInParamsNames.Split(arrDelimiter, StringSplitOptions.None);
        //    string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.None);
        //    try
        //    {
        //        if (RafDB.clsDbGlobal.runSp_ScalarQuery_NoReturn("InsertUpdateSekerRow", arrInParamsNames, arrInParamsValues) != "")
        //        {
        //            string msg = "����� ������ ����� ������� : \n" +
        //                    "�� ������ ���� ���������� ����� �������";
        //            Program.writeToScreenAndToLog(msg, true);
        //            Program.writeToTasksLogTableForRow(msg, 1,7);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = "����� ����� : \n"
        //        + " - ����� = " + ex.Message + "\n"
        //        + " - ������ = " + ex.StackTrace;
        //        Program.writeToScreenAndToLog(msg, true);
        //        Program.writeToTasksLogTableForRow(msg, 1,7);
        //    }

        //}


        static private void NAR(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }




        #region FIND EXCEL WORKSHEET by workSheet name
        static public Excel.Worksheet FindExcelWorksheetByName(Excel.Workbook wk, string worksheetName)
        {
            Excel.Worksheet tempWorkSheet;
            if (wk.Sheets != null)
            {
                for (int i = 1; i <= wk.Sheets.Count; i++)
                {
                    tempWorkSheet = (Excel.Worksheet)wk.Sheets.get_Item(i);
                    if (tempWorkSheet.Name.Equals(worksheetName))
                    {
                        return tempWorkSheet;
                    }
                }
            }
            return null;
        }
        #endregion

        #region GET RANGE

        public string[] GetRange(Excel.Worksheet excelSheet, string rangeCol1,
                                string rangeRow1, string rangeCol2, string rangeRow2)
        {
            workingRangeCells = excelSheet.get_Range(rangeCol1 + rangeRow1 + ":" +
                                                                rangeCol2 + rangeRow2, Type.Missing);
            //workingRangeCells.Select();
            System.Array array = (System.Array)workingRangeCells.Cells.Value2;
            string[] arrayValues = ConvertToStringArray(array, rangeCol1, rangeRow1, rangeCol2, rangeRow2);
            return arrayValues;
        }
        #endregion


        #region GetRange value2 or formula  AsArray

        public Array GetRangeAsArray(Excel.Worksheet excelSheet, string rangeString, Boolean blnGetFormula)
        {

            workingRangeCells = excelSheet.get_Range(rangeString, Type.Missing);
            //workingRangeCells.Select();
            if (blnGetFormula)
            {
                return (System.Array)workingRangeCells.Cells.Formula;
            }
            else
            {
                return (System.Array)workingRangeCells.Cells.Value2;
            }
        }
        #endregion


        static private string getOffsetForGettingCellName(string strNumOrLetter, int numOffset, string strRowOrCol)
        {
            int numResult = 0;
            if (strRowOrCol == "row")
            {
                numResult = numOffset + Convert.ToInt32(strNumOrLetter) - 1;
                return numResult.ToString();
            }
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            if (strRowOrCol == "col")
            {
                if (strNumOrLetter.Length > 2)
                {
                    throw new Exception("more then two letters on the start column , not supported !!!");
                }
                int intGetNumberFromColLetter = 0;
                for (int idx = 0; idx < strNumOrLetter.Length; idx++)
                {
                    intGetNumberFromColLetter = Convert.ToChar(strNumOrLetter.Substring(idx, 1)) - 64;
                    if (strNumOrLetter.Length == 2 && idx == 0)
                    {
                        numResult += intGetNumberFromColLetter * 26;
                    }
                    else
                    {
                        numResult += intGetNumberFromColLetter + numOffset - 1;
                    }
                }
            }
            return numResult.ToString();
        }


        static private string getCellName(int row, int col)
        {
            if (col > 1566)
            {
                throw new Exception("more then two letters on the start column , not supported !!!");
            }

            string result;
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            int offset = 0;
            while (col > 26)
            {
                col -= 26;
                offset++;
            }
            result = "";
            if (offset > 0)
            {
                result += letters[offset - 1];
            }
            result += letters[col - 1];
            result += row.ToString();
            return (result);
        }






        #region ConvertToStringArray
        private string[] ConvertToStringArray(System.Array values, string rangeCol1,
                                         string rangeRow1, string rangeCol2, string rangeRow2)
        {
            string[] newArray = new string[values.Length];

            int index = 0;
            for (int i = values.GetLowerBound(0); i <= values.GetUpperBound(0); i++)
            {
                for (int j = values.GetLowerBound(1); j <= values.GetUpperBound(1); j++)
                {
                    if (values.GetValue(i, j) == null)
                    {
                        newArray[index] = " " + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    else
                    {
                        newArray[index] = values.GetValue(i, j).ToString() + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    index++;
                }
            }
            return newArray;
        }
        #endregion
    }
}
