using System;

namespace giyusConsole
{
	/// <summary>
	/// Summary description for Hebrew.
	/// </summary>
	public sealed class Hebrew
	{
		static bool sIsIE = true;
		static bool sIsNT = true;


        public static string reverseHebrewWordsOrderWhenLastIsNum(string pStrIn)
        {
            string[] arrSpaceDelimiter = new string[1] { " " };
            string res = "";
            string[] arrWords = pStrIn.Split(arrSpaceDelimiter, StringSplitOptions.RemoveEmptyEntries);
            //   idx<arrWords.Length-1 -  to skip last word, because it is a number
            for (int idx = 1;idx<arrWords.Length; idx++)   
            {
                res += arrWords[idx] + " ";
            }

            //at the end, replace direction of '
            //if it is in the start
            if (res.Contains("'"))
            {
                if (res.StartsWith("'"))
                {
                    // 1 - to skip the '
                    //-2 to remove from the end the space
                    res = res.Substring(1,res.Length-2) + "' ";
                }
            }
            return res + arrWords[0];
        }

		static Hebrew()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private static string GetLine(string pStrIn, int n){
			int i = pStrIn.IndexOf(" ", n);
			if(i == -1){i = pStrIn.Length;};
			return pStrIn.Substring(0, i);
		}

		private static string VisualHtml(string pStrIn, int n){
			string s = pStrIn;
			string pVisualHtml = "";
			if(!sIsIE){
				while(s != ""){
					string tmps = GetLine(s, n);
					pVisualHtml = pVisualHtml + Visual(tmps);
					if(tmps.Length < s.Length){
						pVisualHtml = pVisualHtml + "<BR>";
						s = s.Substring(s.Length - tmps.Length);
					}else{
						s = "";
					}
				}
			}else{
				pVisualHtml = pStrIn;
			}
			return pVisualHtml;
		}

		private static string VisualWinHtml(string pStrIn, int n){
			if(! (sIsNT || sIsIE)){
				return pStrIn;
			}else{
				return VisualHtml(pStrIn, n);
			}
		}
		/// <summary>
		/// Handel Parenthesis direction (does not work for multi parenthesis).
		/// </summary>
		/// <param name="pString"></param>
		/// <returns></returns>
		public static string Parenthesis(string pString){
			string pNewStr = pString;
			for(int i = 0; i < pString.Length; i++){
				if(pString.Substring(i,1) == "("){
					pNewStr = pString.Substring(0,pString.IndexOf("(")) + ")" + pString.Substring(pString.IndexOf("(")+1);
				}else if(pString.Substring(i,1) == ")"){
					pNewStr = pNewStr.Substring(0,pString.IndexOf(")")) + "(" + pString.Substring(pString.IndexOf(")")+1);
				}
			}
			return pNewStr;
		}

		public static string DeleteParenthesis(string pString){
			pString = pString.Replace("("," ");
			pString = pString.Replace(")"," ");
			return pString;
		}

		public static string ReplaceAsterixForParenthesis(string pString){
			pString = pString.Replace("(","***");
			pString = pString.Replace(")","***");
			return pString;
		}

		public static string Visual(string pStrIn){
			//yaniv remarked becuase of underline line after hebrew line
//			if(!IsHebrew(pStrIn)){
//				return pStrIn;
//			}

			int i = 0;
			string strTemp = "";
			string strOut = "";
			string LastMode = "";

			int strl = pStrIn.Length;
			while(i < strl){
				char c = pStrIn[i];
				if(IsHebrew(c)){
					i = GetHebRun(pStrIn, ref strTemp, ref i);
					strOut = StrReverse(RevPairs(strTemp)) + strOut;
					LastMode = "heb";
				}else if(IsEnglish(c)){
					i = GetEngRun(pStrIn, ref strTemp, ref i);
					strOut = strTemp + strOut;
					LastMode = "eng";
				}else if(IsNumber(c)){
					i = GetNumRun(pStrIn, ref strTemp, ref i);
					strOut = strTemp + strOut;
					LastMode = "num";
				}else{
					i = GetNeutralRun(pStrIn, ref strTemp, ref i);
					strOut = StrReverse(RevPairs(strTemp)) + strOut;
					LastMode = "neu";
				}
				i++;
			}
			return strOut;
		}


		private static string RevPairs(string pStrIn){
			string strOut = "";
			for(int i=0 ; i<pStrIn.Length; i++){
				char c = pStrIn[i];
				switch(c){
					case ')': c = '('; break;
					case '(': c = ')'; break;
					case ']': c = '['; break;
					case '[': c = ']'; break;
		            
					case '}': c = '{'; break;
					case '{': c = '}'; break;
					case '>': c = '<'; break;
					case '<': c = '>'; break;
				}
				strOut += c;
			}
			return strOut;
		}			

		private static int GetEngRun(string pStrIn, ref string strOut, ref int i){
			bool done = false;		    
			int strl = pStrIn.Length;
			string strTemp = "";
			int pos = 0;
		    
			while(i < strl && !done){
				char c = pStrIn[i];
				if(IsEnglish(c) || IsNumber(c)){
					pos = i;
					strTemp = strTemp + c;
					strOut = strTemp;
				}else if(IsHebrew(c)){
					done = true;
				}else{
					strTemp = strTemp + c;
				}
				i++;
			}
			return pos;
		}

		private static int GetHebRun(string pStrIn, ref string strOut, ref int i){
			bool done = false;		    
			int strl = pStrIn.Length;
			string strTemp = "";
			int pos = 0;

			while(i < strl && !done){
				char c = pStrIn[i];
				if(IsHebrew(c)){
					pos = i;
					strTemp = strTemp + c;
					strOut = strTemp;
				}else if(IsEnglish(c) || IsNumber(c)){
					done = true;
				}else{
					strTemp = strTemp + c;
				}
				i++;
			}
			return pos;
		}

		private static int GetNumRun(string pStrIn, ref string strOut, ref int i){
			bool done = false;		    
			int strl = pStrIn.Length;
			string strTemp = "";
			bool LastWasNeutral = false;
			int pos = 0;
		    
			while(i < strl && !done){
				char c = pStrIn[i];

				if(IsNumber(c) || IsEnglish(c)){
					pos = i;
					strTemp = strTemp + c;
					strOut = strTemp;
					LastWasNeutral = false;
				}else if(!LastWasNeutral && IsNeutral(c)){
					strTemp = strTemp + c;
					LastWasNeutral = true;
				}else{
					done = true;
				}
				i++;
			}
			return pos;
		}

		private static int GetNeutralRun(string pStrIn, ref string strOut, ref int i){
			bool done = false;		    
			int strl = pStrIn.Length;
			string strTemp = "";
			bool LastWasNeutral = false;
			int pos = 0;
		    
			while(i < strl && !done){
				char c = pStrIn[i];
				if(!(IsHebrew(c) || IsEnglish(c) || IsNumber(c))){
					pos = i;
					strTemp = strTemp + c;
					strOut = strTemp;
				}else{
					done = true;
				}
				i++;
			}
			return pos;
		}

		private static bool IsNumber(char c){
			return ((c >= '0' && c <= '9') ||
					("%$".IndexOf(c) > -1));
		}

		private static bool IsHebrew(char c){
			return (c >= '�' && c <= '�');
			//Or CBool(InStr(1, "?()[]{}.", c))
		}

		private static bool IsEnglish(char c){
			return ((c >= 'a' && c <= 'z') ||
					(c >= 'A' && c <= 'Z'));
			//Or CBool(InStr(1, "?()[]{}.", c))
		}

		private static bool IsNeutral(char c){
			return (@".,/:-+$() ".IndexOf(c) > -1);
		}

		private static bool IsHebrew(string s){
			for(int i=0; i<s.Length; i++){
				if(IsHebrew(s[i])){
					return true;
				}
			}
			return false;
		}

		private static string StrReverse(string s){
			char[] c = s.ToCharArray();
			Array.Reverse(c);
			return new string(c);
		}

        /// <summary>
        /// translate encoded text to hebrew
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
		public static string GetReceivedString(string str)
		{
			System.Collections.Hashtable HebPs = new System.Collections.Hashtable();
			string tmp = "���������������������������";
			for(int i=0;i<tmp.Length;i++)
			{
				string curNum = "&#x"+  ((int)(i+1488)).ToString("x") + ";";
				HebPs.Add(curNum,tmp[i]);
			}
			string retVal = "";
			//01234567890123
			//&#x5d1;&#x5d9;&#x5e8;&#x5dd; &#x5e9;&#x5de;&#x5d9;&#x5e8; &#x5ea;&#x5de;&#x5e8;
			int charSt = str.IndexOf("&#x");
			while(charSt > -1)
			{
				string CurChar ="";
				string CurCharH = "";
				int charEd = 0;
				if (charSt > -1)
				{
					charEd = str.IndexOf(";",charSt)+1;
					CurCharH = str.Substring(charSt,7);
					CurChar = HebPs[CurCharH].ToString();
					//str = str.Substring(0,charSt)+CurChar+str.Substring(charEd);
					str = str.Replace(CurCharH,CurChar);
				}
				charSt = str.IndexOf("&#x");
			}
			HebPs = null;

			return str;
		}

		private static string decToHex(long num)
		{
			string hexNum = "0123456789abcdef";
			string retVal="";
			long curDigit;

			while(num>0)
			{
				Math.DivRem(num,16, out curDigit);
				retVal = hexNum[(int)curDigit] + retVal;
				num= Math.DivRem(num,16, out curDigit);
			}
			return retVal;
		}

	}
}
