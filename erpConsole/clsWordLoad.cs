using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using msWord = Microsoft.Office.Interop.Word;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.IO;
using giyusBL;

namespace giyusConsole
{
    class clsWordLoad
    {
        Microsoft.Office.Interop.Word.Application varWord;
        Microsoft.Office.Interop.Word.Document varDoc;
        Microsoft.Office.Interop.Word.Range varRange;
        Microsoft.Office.Interop.Word.Bookmark varBookMark;

        public static string[] arrDelimiter = new string[1] { "!$!" };

        object strNewFile = "";

        object objUnitWdChar = msWord.WdUnits.wdCharacter;
        object objUnitWdLine = msWord.WdUnits.wdLine;
        object objUnitWord = msWord.WdUnits.wdWord;
        object objUnitWdTable = msWord.WdUnits.wdTable;
        object objUnitWdStory = msWord.WdUnits.wdStory;
        object objUnitCell = msWord.WdUnits.wdCell;
        object objUnitRow = msWord.WdUnits.wdRow;
        object objUnitPar = msWord.WdUnits.wdParagraph;
        object objFalse = false;
        object objTrue = true;
        object objMissing = Type.Missing;
        msWord.TextInput wordInput;

        object objCountOne = 1;
        object objCountTwo = 2;
        object objCountThree = 3;
        object objCountFour = 4;
        int intGradeCounter = 0;
        int intProp2RowsCount = 0;

        Boolean blnWordVisible = Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);
        Boolean blnWaitForKey = Convert.ToBoolean(ConfigurationManager.AppSettings["waitForKey"]);
        object objPass = null;
        string msg = "";


        public void TypeTextBookMarkAndLeaveInputField(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            if (!varDoc.Bookmarks.Exists(bkName))
            {
                return;
            }
            //varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            //varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToField, objMissing, objMissing, bkName).Select();
            varDoc.FormFields.get_Item(bkName).TextInput.Default = customerValue;
            //varDoc.FormFields.set_Item(bkName).Select();


            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //varWord.Selection.MoveLeft(msWord.WdUnits.wdCharacter, 1, objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }



        public void addBookMark(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, bkName);
            varWord.Selection.TypeText("abc");
            //varDoc.FormFields.get_Item(1).Range.Select();
            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }
        protected string cleanString(string myStr)
        {
            return myStr.Replace("\r\a", "").Trim().Replace("\r", " ").ToLower();
        }

        public void fillBookMark(string strFieldName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            string strBookMark = "";
            for (int idxCounter = 1; idxCounter <= 3; idxCounter++)
            {
                strBookMark = strFieldName + idxCounter.ToString();
                if (varDoc.Bookmarks.Exists(strBookMark))
                {
                    object oBookMark = strBookMark;
                    msWord.Bookmark bkMark = varDoc.Bookmarks.get_Item(ref oBookMark);
                    bkMark.Select();
                    if (customerValue == "")
                    {
                        customerValue = "-";
                    }
                    varWord.Selection.TypeText(customerValue);


                    //preserve nid1 bookmark with nid1 seleted text to get nid from each doc for pdf file
                    if (strBookMark == "nid1")
                    {
                        varWord.Selection.MoveLeft(msWord.WdUnits.wdCharacter, 9, objTrue);
                        varDoc.Bookmarks.Add(strBookMark, varWord.Selection);
                    }
                    
                    //msWord.Bookmark bm = varDoc.Bookmarks[strBookMark];
                    //msWord.Range range = bm.Range.Duplicate;
                    //bm.Range.Text = customerValue;
                    //range
                    //object oBookMark = strBookMark;
                    //varDoc.Bookmarks.get_Item(ref oBookMark).Range.Text = customerValue;
                    //varDoc.Bookmarks[strBookMark].Range.Text = customerValue;

                }
            }
        }


        public void addFormField(string bkName, string defaultValue)
        {
            varDoc.FormFields.Add(varWord.Selection.Range, msWord.WdFieldType.wdFieldFormTextInput);
            varDoc.FormFields.get_Item("Text1").Name = bkName;
            varDoc.FormFields.get_Item(bkName).TextInput.Default = defaultValue;

        }


        public void addEmptyCheckBoxField()
        {
            varDoc.FormFields.Add(varWord.Selection.Range, msWord.WdFieldType.wdFieldFormCheckBox);
            //varDoc.FormFields.get_Item("Text1").Name = bkName;
            //varDoc.FormFields.get_Item(bkName).TextInput.Default = defaultValue;

        }


        public void loadDoc()//string emp_num, string strFileName, string resRole)
        {
            try
            {
                object varFileName = clsWord.strWordInputDir + "\\" + clsWord.strFileName;

                // Create a reference to MS Word application
                varWord = new Microsoft.Office.Interop.Word.Application();

                // Open word
                varWord.Visible = blnWordVisible;
                if (blnWordVisible)
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsAll;
                }
                else
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;
                }
                // Creates a reference to a word document
                msg = "����� ������ �����: = " + varFileName;
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTable(msg, 2, 1);


                objPass = Missing.Value;
                if (!File.Exists(varFileName.ToString()))
                {
                    msg = "�� ���� ����� ����� ���� ����";
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTable(msg, 1, 1);
                    return;
                }

                object objFmt = msWord.WdOpenFormat.wdOpenFormatAuto;
                varDoc =
                    varWord.Documents.Open(ref varFileName, ref objMissing, ref objFalse,
                       ref objMissing, ref objPass, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objFmt,
                       ref objMissing, ref objMissing, ref objFalse, ref objMissing, ref objMissing, ref objMissing);

                // Activate the document
                varDoc.Activate();
                //for (int idx = 0; idx < varDoc.Bookmarks.Count; idx++)
                //{
                string strFileNoExt = Path.GetFileNameWithoutExtension(varFileName.ToString());

                Console.WriteLine("start bookmarks file=" + varFileName + " - time=" + DateTime.Now);
                if (clsWord.dtCustomer.Rows.Count > 0) //customerId param came from a taskOrder (from program.cs - loadWord option)
                {
                    if (strFileNoExt == "tax")
                    {
                        #region taxFile
                        TypeTextBookMarkAndLeaveInputField("yearStartOpened", (DateTime.Now.Year - 6).ToString());//prev 6 years
                        TypeTextBookMarkAndLeaveInputField("yearEndOpened", (DateTime.Now.Year - 1).ToString());//prev year
                        TypeTextBookMarkAndLeaveInputField("fullNameOpened",
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_lastName"]) + " " +
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_firstName"]));
                        TypeTextBookMarkAndLeaveInputField("mateNameOpened",
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateLastName"]) + " " +
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateName"]));
                        TypeTextBookMarkAndLeaveInputField("mateNidOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]));
                        TypeTextBookMarkAndLeaveInputField("nidOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_nid"]));
                        //same for mate
                        string strAddress = clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_street"]) + " " +
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_houseNum"]) + " " +
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_city"]) + " " +
                                clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_zip"]);
                        TypeTextBookMarkAndLeaveInputField("addressOpened", strAddress);
                        //same for mate
                        string strCellPhone = clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_cell"]);
                        TypeTextBookMarkAndLeaveInputField("cellOpened", strCellPhone);
                        //same for mate
                        string strEmail = clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_email"]);
                        TypeTextBookMarkAndLeaveInputField("eMailOpened", strEmail);

                        TypeTextBookMarkAndLeaveInputField("dobOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_birthDate"]));

                        TypeTextBookMarkAndLeaveInputField("stateOpened", switchState(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_stateName"])));

                        TypeTextBookMarkAndLeaveInputField("stateDateOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_stateDate"]));

                        string strCustomerChildsCountTo18 = getCustomerChildsCountTo18();
                        TypeTextBookMarkAndLeaveInputField("chldCnt18Opened", strCustomerChildsCountTo18);

                        if (clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]) != "")
                        {
                            TypeTextBookMarkAndLeaveInputField("mateEMailOpened", strEmail);
                            TypeTextBookMarkAndLeaveInputField("mateAddressOpened", strAddress);
                            TypeTextBookMarkAndLeaveInputField("mateCellOpened", strCellPhone);
                            TypeTextBookMarkAndLeaveInputField("mateStateNameOpened", switchState(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateStateName"])));
                            TypeTextBookMarkAndLeaveInputField("mateDobOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateBirthDate"]));
                            TypeTextBookMarkAndLeaveInputField("mateStateDateOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_MateMarrigeDate"]));
                            TypeTextBookMarkAndLeaveInputField("mateChldCnt18Opened", strCustomerChildsCountTo18);
                        }
                        #endregion taxFile
                    }
                    else
                    {
                        #region else - all other files
                        fillBookMark("firstName", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_firstName"]));
                        fillBookMark("lastName", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_lastName"]));
                        fillBookMark("street", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_street"]));
                        fillBookMark("dob", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_birthDate"]));
                        fillBookMark("id", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_id"]));
                        fillBookMark("nid", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_nid"]));
                        fillBookMark("phone", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_phone"]));
                        fillBookMark("cell", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_cell"]));
                        fillBookMark("sex", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_sex"]));
                        fillBookMark("today", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                        fillBookMark("zip", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_zip"]));
                        fillBookMark("houseNum", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_houseNum"]));
                        fillBookMark("city", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_city"]));
                        fillBookMark("fax", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_fax"]));
                        fillBookMark("workPhone", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_WorkPhone"]));
                        fillBookMark("td", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_td"]));
                        fillBookMark("apartmentNum", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_ApartmentNum"]));
                        fillBookMark("eMail", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_email"]));
                        fillBookMark("startDate", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_startDate"]));
                        fillBookMark("endDate", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_endDate"]));

                        TypeTextBookMarkAndLeaveInputField("branchName1", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_bankBranch"]));
                        TypeTextBookMarkAndLeaveInputField("branchNum1", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_bankBranchNum"]));
                        TypeTextBookMarkAndLeaveInputField("bank1", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_bank"]));
                        TypeTextBookMarkAndLeaveInputField("account1", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_accountNo"]));

                        fillBookMark("mateFirstName", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateName"]));
                        fillBookMark("mateLastName", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateLastName"]));
                        fillBookMark("mateNid", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]));

                        if (clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]) != "")
                        {
                            fillBookMark("mateStateName", switchState(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateStateName"])));
                            fillBookMark("mateSex", switchSex(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateSex"])));
                            fillBookMark("mateDob", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateBirthDate"]));
                            fillBookMark("mateMarrigeDate", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_MateMarrigeDate"]));
                        }
                        else
                        {
                            fillBookMark("mateStateName", "");
                            fillBookMark("mateSex", "");
                            fillBookMark("mateDob", "");
                            fillBookMark("mateMarrigeDate", "");
                        }

                        fillBookMark("address",
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_street"]) + " " +
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_houseNum"]) + " " +
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_city"]) + " " +
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_zip"]));

                        fillBookMark("fullName",
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_lastName"]) + " " +
                            clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_firstName"]));

                        if (strFileNoExt == "harel_keren")
                        {

                            TypeTextBookMarkAndLeaveInputField("addressOpened",
                                    clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_street"]) + " " +
                                    clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_houseNum"]) + " " +
                                    clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_city"]) + " " +
                                    clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_zip"]));

                        }
                        else if (strFileNoExt == "fenix_madaness")
                        {

                            TypeTextBookMarkAndLeaveInputField("nidOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_nid"]));
                            TypeTextBookMarkAndLeaveInputField("firstNameOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_firstName"]));
                            TypeTextBookMarkAndLeaveInputField("lastNameOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_lastName"]));
                            TypeTextBookMarkAndLeaveInputField("dobOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_birthDate"]));
                            TypeTextBookMarkAndLeaveInputField("sexOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_sex"]));

                            TypeTextBookMarkAndLeaveInputField("mateFirstNameOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateName"]));
                            TypeTextBookMarkAndLeaveInputField("mateLastNameOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateLastName"]));
                            TypeTextBookMarkAndLeaveInputField("mateNidOpened", clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]));
                            if (clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateNid"]) != "")
                            {
                                TypeTextBookMarkAndLeaveInputField("mateSexOpened", switchSex(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateSex"])));
                                TypeTextBookMarkAndLeaveInputField("mateDobOpened", clsUtil.convertDateNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_mateBirthDate"]));
                            }

                            if (clsWord.dtCustomerChild != null && clsWord.dtCustomerChild.Rows.Count > 0)
                            {
                                fillChildMadaness(clsWord.dtCustomerChild.Rows.Count, true);
                            }
                        }
                        else if (strFileNoExt == "harel_teeth_family")
                        {

                            //do this only if there are childs on DB and childStart bookmark exist in current document
                            if (clsWord.dtCustomerChild != null && clsWord.dtCustomerChild.Rows.Count > 0)
                            {
                                string CustomerChildsCountAbove5Under23 = getCustomerChildsCountAbove5Under23();
                                DataTable dtCustomerChildsFrom23To30 = getCustomerChildsFrom23To30();
                                DataTable dtCustomerChildsTo23 = getCustomerChildsTo23();
                                if (dtCustomerChildsTo23 != null && dtCustomerChildsTo23.Rows.Count > 0)
                                {
                                    fillChild("To23", dtCustomerChildsTo23.Rows.Count, dtCustomerChildsTo23, false);
                                }
                                if (dtCustomerChildsFrom23To30 != null && dtCustomerChildsFrom23To30.Rows.Count > 0)
                                {
                                    fillChild("From23To30", dtCustomerChildsFrom23To30.Rows.Count, dtCustomerChildsFrom23To30, true);
                                }
                                fillBookMark("kidsAbove5Under23", CustomerChildsCountAbove5Under23);
                            }
                            else
                            {
                                fillBookMark("kidsAbove5Under23", "0");
                            }
                        }
                        else if (strFileNoExt == "pensya")
                        {
                            if (clsWord.dtCustomerChild != null && clsWord.dtCustomerChild.Rows.Count > 0)
                            {
                                fillChildPensya(clsWord.dtCustomerChild.Rows.Count, true);
                            }

                        }
                        else if (strFileNoExt == "101")
                        {
                            DataTable dtCustomerChildsTo19 = getCustomerChildsTo19();
                            if (dtCustomerChildsTo19 != null && dtCustomerChildsTo19.Rows.Count > 0)
                            {
                                fillBookMark("kidsUnder19", dtCustomerChildsTo19.Rows.Count.ToString());
                                TypeTextBookMarkAndLeaveInputField("yearOpened1", DateTime.Now.Year.ToString());

                                TypeTextBookMarkAndLeaveInputField("phone1Opened1", "0373-9999");
                                TypeTextBookMarkAndLeaveInputField("phone2Opened1", "03-7349999");
                                TypeTextBookMarkAndLeaveInputField("text16", "��"); //citizen

                                fillChild101(dtCustomerChildsTo19);
                            }

                            //fillBookMark("kidsAbove5Under23", CustomerChildsCountAbove5Under23);
                        }
                        fillBookMark("state", switchState(clsUtil.convertNullToEmpty(clsWord.dtCustomer.Rows[0]["customer_stateName"])));
                        #endregion else - all other files
                    }
                    Console.WriteLine("end bookmarks=" + DateTime.Now + "\n");
                    strNewFile = clsWord.strWordOutputDir + "\\files\\" + strFileNoExt + "_" + clsWord.customerID + ".doc";
                }

                object format = msWord.WdSaveFormat.wdFormatXMLDocumentMacroEnabled;
                varDoc.Protect(msWord.WdProtectionType.wdAllowOnlyFormFields, objMissing, "", objMissing, objMissing);
                varDoc.SaveAs(ref strNewFile, ref format,
                    Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                varWord.Quit(ref objMissing, ref objMissing, ref objMissing);
                msg = "������ ������ !";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTable(msg, 2, 1);
            }
            catch (Exception varE)
            {
                msg = "�����: " + varE.Message + "<br>";
                Program.writeToScreenAndToLog(msg + varE.StackTrace, true);
                Program.writeToTasksLogTable(msg, 1, 1);
                varWord.Quit(ref objFalse, ref objMissing, ref objMissing);
                msg = "������ ������ !";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTable(msg, 2, 1);
            }

        }





        public void fillChild101(DataTable dt)
        {
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");

            for (int idx = 0; idx < dt.Rows.Count; idx++)
            {
                for (int idxCol = 0; idxCol < dt.Columns.Count; idxCol++)
                {
                    if (idxCol == 0)
                    {
                        if (idx > 0) //Not first line, so open the next line
                        {
                            varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        }
                        addEmptyCheckBoxField();
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        varWord.Selection.TypeText(clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]));
                    }
                    else if (idxCol == 1)
                    {
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        varWord.Selection.TypeText(clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]));
                    }
                    else if (idxCol == 2)
                    {
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        varWord.Selection.TypeText(clsUtil.convertDateNullToEmpty(dt.Rows[idx][idxCol]));
                    }
                }
            }
        }


        //private void fillChild101(DataTable dt)
        //{
        //    //if (dt.Rows.Count > 2)
        //    //{
        //    //    varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
        //    //    varWord.Selection.InsertRowsBelow(((dt.Rows.Count - 2) / 2) + ((dt.Rows.Count - 2) % 2)); //add rows
        //    //}
        //    varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
        //    addChilds101(dt);
        //}







        private void fillChildMadaness(int intChildCount, Boolean blnAddFormField)
        {
            //Selection.MoveRight Unit:=wdCharacter, Count:=8, Extend:=wdExtend
            //Selection.Copy
            //Selection.MoveDown Unit:=wdLine, Count:=1
            //Selection.Paste
            if (intChildCount > 1)
            {
                varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
                varWord.Selection.InsertRowsBelow(intChildCount - 1); //add rows
            }
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
            addChildCells(clsWord.dtCustomerChild, blnAddFormField, true);
        }


        private void fillChildPensya(int intChildCount, Boolean blnAddFormField)
        {
            //Selection.MoveRight Unit:=wdCharacter, Count:=8, Extend:=wdExtend
            //Selection.Copy
            //Selection.MoveDown Unit:=wdLine, Count:=1
            //Selection.Paste
            if (intChildCount > 1)
            {
                varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
                varWord.Selection.InsertRowsBelow(intChildCount - 1); //add rows
            }
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart");
            addChildCells(clsWord.dtCustomerChild, blnAddFormField, false, true);
        }

        private string getCustomerChildsCountTo18()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.ExecuteSQLScalar("[giyus].getCustomerChildsCountTo18", arrInParamsNames, arrInParamsValues);
        }


        private string getCustomerChildsCountAbove5Under23()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.ExecuteSQLScalar("[giyus].getCustomerChildsCountAbove5Under23", arrInParamsNames, arrInParamsValues);
        }

        private DataTable getCustomerChildsFrom23To30()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.FillDt("[giyus].getCustomerChildsFrom23To30", arrInParamsNames, arrInParamsValues);
        }

        private DataTable getCustomerChildsTo23()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.FillDt("[giyus].getCustomerChildsTo23", arrInParamsNames, arrInParamsValues);
        }


        private DataTable getCustomerChildsTo19()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.FillDt("[giyus].getCustomerChildsTo19", arrInParamsNames, arrInParamsValues);
        }

        private void fillChild(string strType, int intChildCount, DataTable dt, Boolean blnAddFormField)
        {
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart" + strType);
            if (intChildCount > 1)
            {
                varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter, 5, msWord.WdMovementType.wdExtend);
                varWord.Selection.InsertRowsBelow(intChildCount - 1); //add rows
                varWord.Selection.MoveLeft(msWord.WdUnits.wdCharacter, 1, msWord.WdMovementType.wdMove);
                varWord.Selection.MoveDown(msWord.WdUnits.wdLine, intChildCount - 1, msWord.WdMovementType.wdMove);
                varWord.Selection.MoveUp(msWord.WdUnits.wdLine, 1, msWord.WdMovementType.wdMove);
                varWord.Selection.MoveUp(msWord.WdUnits.wdLine, intChildCount - 1, msWord.WdMovementType.wdExtend);
                varWord.Selection.Cells.Merge();//merge right cell
                varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart" + strType);
                //move left untill first field of added child row
                varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
            }
            else
            {
                //only one row - so skeep the rightest column with desc of type of children
                varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
            }
            addChildCells(dt, blnAddFormField, false);
        }





        private string switchState(string customer_stateName)
        {
            switch (customer_stateName)
            {
                case "1003":
                    customer_stateName = "����";
                    break;
                case "1004":
                    customer_stateName = "����";
                    break;
                case "1005":
                    customer_stateName = "����";
                    break;
                case "1006":
                    customer_stateName = "����";
                    break;
                case "1007":
                    customer_stateName = "���� ������";
                    break;
                default:
                    customer_stateName = "����";
                    break;
            }
            return customer_stateName;
        }
        private string switchSex(string strText)
        {
            if (strText == "1001" || strText == "")
            {
                return "���";
            }
            else //if (strText == "1002")
            {
                return "����";
            }
        }

        public void addChildCells(DataTable dt, Boolean blnAddFormField, Boolean blnIsMadaness, Boolean blnIsPensya = false)
        {
            string strText = "";
            string strFirstName = "";
            string strLastName = "";
            string[] arrName = null;
            for (int idx = 0; idx < dt.Rows.Count; idx++)
            {
                for (int idxCol = 0; idxCol < dt.Columns.Count; idxCol++)
                {
                    if (idxCol <= 1)
                    {
                        if (blnIsMadaness || blnIsPensya)
                        {
                            if (idxCol == 0)
                            {
                                varWord.Selection.Text = "��� " + (idx + 1).ToString();
                                varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                                strText = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]);
                                addFormField("childNid" + idx + "_" + idxCol, strText);
                            }
                            else if (idxCol == 1)
                            {
                                arrName = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]).Split(' ');
                                strLastName = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]).Split(' ')[arrName.Length - 1];//last name from end
                                strFirstName = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]).Substring(0,
                                            clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]).Length - strLastName.Length);//all other is first name
                                addFormField("childLast" + idx + "_" + idxCol, strFirstName);
                                varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                                addFormField("childFirst" + idx + "_" + idxCol, strLastName);
                            }
                        }
                        else
                        {
                            strText = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]);
                            if (blnAddFormField)
                            {
                                addFormField("child" + idx + "_" + idxCol, strText);
                            }
                            else
                            {
                                varWord.Selection.Text = strText;
                            }
                        }
                    }
                    else if (idxCol == 2)
                    {
                        strText = clsUtil.convertDateNullToEmpty(dt.Rows[idx][idxCol]);
                        if (blnAddFormField)
                        {
                            addFormField("child" + idx + "_" + idxCol, strText);
                        }
                        else
                        {
                            varWord.Selection.Text = strText;
                        }
                    }
                    else if ((!blnIsPensya) && (idxCol == 3))
                    {
                        strText = clsUtil.convertNullToEmpty(dt.Rows[idx][idxCol]);
                        strText = switchSex(strText);
                        if (blnAddFormField)
                        {
                            addFormField("child" + idx + "_" + idxCol, strText);
                        }
                        else
                        {
                            varWord.Selection.Text = strText;
                        }
                    }
                    if (!blnIsPensya)//not pensyia dot right after every column
                    {
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                    }
                    else if (blnIsPensya && idxCol < 3) // in pensya we don't have the sex column, so don't do one column right after drawing.
                    {
                        //in last line and last column of childs in pensya, 
                        //when idx == dt.Rows.Count-1, don't make new row after last column
                        if (idxCol == 2 && idx == dt.Rows.Count - 1)
                        {
                        }
                        else
                        {
                            varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        }
                    }

                    if (idxCol == 3 && blnIsMadaness)
                    {
                        addEmptyCheckBoxField();
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                        addEmptyCheckBoxField();
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                    }

                }
                if ((!blnIsMadaness) && (!blnIsPensya)) //we have right full cell only in harel_teeth,in madaness we write child1,2,3...
                {
                    varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                }
            }
        }



        private Boolean Word_FindStr(string strToFind, Boolean blnForWard)
        {

            //english - so if we used font barcod in english
            //it is returning to default font
            //resolve a bug !!!

            //			varWord.Keyboard (1033);		//eng

            //hebrew
            varWord.Keyboard(1037);
            object Miss = Missing.Value;
            varWord.Selection.Find.ClearFormatting();
            varWord.Selection.Find.Text = strToFind;
            //varWord.Selection.Find.Replacement.Text = NewStr;
            varWord.Selection.Find.Forward = blnForWard;
            varWord.Selection.Find.Wrap = msWord.WdFindWrap.wdFindContinue;
            varWord.Selection.Find.Format = true;
            varWord.Selection.Find.MatchCase = false;
            varWord.Selection.Find.MatchWholeWord = false;
            varWord.Selection.Find.MatchWildcards = false;
            varWord.Selection.Find.MatchSoundsLike = false;
            varWord.Selection.Find.MatchAllWordForms = false;
            varWord.Selection.Find.MatchKashida = false;
            varWord.Selection.Find.MatchDiacritics = false;
            varWord.Selection.Find.MatchAlefHamza = false;
            varWord.Selection.Find.MatchControl = false;

            //varWord.Selection.Find.Replacement.Font.Name = DefaultFontName_Hebrew;

            //varWord.Selection.Find.Replacement.Font.Size = DefaultFontSize;

            return varWord.Selection.Find.Execute
                (ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
                ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
                ref Miss, ref Miss, ref Miss, ref Miss, ref Miss);
        }

    }
}