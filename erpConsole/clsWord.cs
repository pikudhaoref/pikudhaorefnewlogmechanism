using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Reflection;
using System.Data;
using giyusBL;

//using Office = Microsoft.Office.Core;
namespace giyusConsole
{
    class clsWord
    {
        public clsWordLoad clsLoad;
        public Excel.Workbook oWorkBook;
        public Excel.ApplicationClass oAppClass = null;
        public Excel.Worksheet oWorkSheet;

        static public DataTable dtCustomer = null;
        static public DataTable dtCustomerChild = null;

        public Excel.Range workingRangeCells = null;
//        static Excel.Range propRange = null;
        public Excel.Range A1MarginRange = null;

        public string[] strMarginSplitter = new string[1] { ";" }; // between margins
        public string[] strRangeSplitter = new string[1] { "!" }; // between each range name and cells
        public string[] yaelSplitter = new string[1] { "!y@" };

        static public string customerID = "";
        static string strData;
        static public string strWordInputDir = "";
        static public string strWordOutputDir = "";
        static string[] strDataSplitter = new string[1];
        static string[] strDataSplitter2 = new string[1];
        //static int lineCount = 0;

        static public string strFileName="";

        Boolean blnWordVisible = Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);


        private DataTable GetCustomer()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.FillDt("[giyus].getCustomer", arrInParamsNames, arrInParamsValues);
        }


        private DataTable GetCustomerChild()
        {
            string strInParamsNames = "customer_id";
            string strInParamsValues = clsWord.customerID;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsUtil.FillDt("[giyus].getCustomerChilds", arrInParamsNames, arrInParamsValues);
        }


        public void doWord()
        {
            try
            {
                string msg = "����� ����� ����� ����� ����";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg, 2, 1);

                dtCustomer = GetCustomer();
                dtCustomerChild = GetCustomerChild();

                strWordInputDir = Program.paramStrWordWorkDir;//ConfigurationManager.AppSettings["wordInputDir"];
                strWordOutputDir = Program.paramStrOutputDir;
                //            strDataSplitter[0] = ",";
                //            strDataSplitter2[0] = "!$!";
                //strConStr = ConfigurationManager.AppSettings["rafManPowerConnectionString"];
                // *******************  make connection ******************************************************
                string[] arrMarginsRanges = null;
                    
                //string[] arrSubData = null;
                //string[] arrSubMarginsRanges;

                CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                oAppClass = new Excel.ApplicationClass();

                //******************************excel Alerts ****************************
                oAppClass.Visible = blnWordVisible;
                oAppClass.DisplayAlerts = blnWordVisible;

                #region excel
                oWorkBook = oAppClass.Workbooks.Open(strWordInputDir + "\\docList.xlsx",
                    Missing.Value, (object)true, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                //******************************excel Alerts ****************************
                // ******************************************************************//
                //get display_Names sheet
                oWorkSheet = FindExcelWorksheetByName(oWorkBook, "Sheet1");
                if (oWorkSheet == null)
                {
                    msg = @"������ ��� - """ + "Sheet1" + @""" �� ����";
                    Program.writeToTasksLogTable(msg, 1,1);
                    throw new Exception(msg);
                }

                Program.lineCount = 2;
                A1MarginRange = oWorkSheet.get_Range("A1:A1", Type.Missing);
                Array arrayMargins = GetRangeAsArray(oWorkSheet, A1MarginRange.Value2.ToString(), false);

                for (int i = arrayMargins.GetLowerBound(0); i <= arrayMargins.GetUpperBound(0); i++)
                {
                    Program.lineCount++;
                    //collect all cells of every margin row and split them to work with array for each range
                    strData = "";
                    for (int j = arrayMargins.GetLowerBound(1); j <= arrayMargins.GetUpperBound(1); j++)
                    {
                        if (arrayMargins.GetValue(i, j) == null)
                        {
                            strData += "" + strMarginSplitter[0];
                        }
                        else
                        {
                            strData += arrayMargins.GetValue(i, j).ToString() + strMarginSplitter[0];
                        }
                    }
                    arrMarginsRanges = strData.Split(strMarginSplitter, StringSplitOptions.None);
                    strFileName = arrMarginsRanges[0];

                    clsLoad = new clsWordLoad();
                    clsLoad.loadDoc();
                    //}
                }
                msg = "����� ����� ����� ����� ������� ������";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg, 2, 1);

                #endregion excel

                #region close all excel
                try
                {
                    // ******************  close the excell documents *******************
                    object objSave = false;
                    object objTrue = true;
                    if (oWorkBook != null)
                    {
                        //close the first workBook
                        //oWorkBook.Close(objSave, (object)strFullFileName, Missing.Value);
                        oWorkBook.Close(objSave, Missing.Value, objTrue);
                        NAR(oWorkBook);
                        oWorkBook = null;
                        Process[] prc = Process.GetProcesses();
                        for (int idx = 0; idx < prc.Length; idx++)
                        {
                            try
                            {
                                if (prc[idx].MainModule.ModuleName.StartsWith("EXCEL"))
                                {
                                    prc[idx].Kill();
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    // ******************  close the excell documents *******************

                }
                catch (Exception ex)
                {
                    msg = "����� ����� : \n"
                                    + " - ����� = " + ex.Message
                                    + "\n - ������ = " + ex.StackTrace;
                    string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message;
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTable(msg2, 1,1);

                }
                #endregion close all excel
            }
            catch (Exception ex)
            {
                string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message
                                + "\n - ������ = " + ex.StackTrace;
                string msg3 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg2, true);
                Program.writeToTasksLogTable(msg3, 1, 1);

            }
            finally
            {
            }
        }



        //put this procedure in your code.

        static private void NAR(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }


        #region FIND EXCEL WORKSHEET by workSheet name
        static public Excel.Worksheet FindExcelWorksheetByName(Excel.Workbook wk, string worksheetName)
        {
            Excel.Worksheet tempWorkSheet;
            if (wk.Sheets != null)
            {
                for (int i = 1; i <= wk.Sheets.Count; i++)
                {
                    tempWorkSheet = (Excel.Worksheet)wk.Sheets.get_Item(i);
                    if (tempWorkSheet.Name.Equals(worksheetName))
                    {
                        return tempWorkSheet;
                    }
                }
            }
            return null;
        }
        #endregion

        #region GET RANGE

        public string[] GetRange(Excel.Worksheet excelSheet, string rangeCol1,
                                string rangeRow1, string rangeCol2, string rangeRow2)
        {
            workingRangeCells = excelSheet.get_Range(rangeCol1 + rangeRow1 + ":" +
                                                                rangeCol2 + rangeRow2, Type.Missing);
            //workingRangeCells.Select();
            System.Array array = (System.Array)workingRangeCells.Cells.Value2;
            string[] arrayValues = ConvertToStringArray(array, rangeCol1, rangeRow1, rangeCol2, rangeRow2);
            return arrayValues;
        }
        #endregion


        #region GetRange value2 or formula  AsArray

        public Array GetRangeAsArray(Excel.Worksheet excelSheet, string rangeString, Boolean blnGetFormula)
        {

            workingRangeCells = excelSheet.get_Range(rangeString, Type.Missing);
            //workingRangeCells.Select();
            if (blnGetFormula)
            {
                return (System.Array)workingRangeCells.Cells.Formula;
            }
            else
            {
                return (System.Array)workingRangeCells.Cells.Value2;
            }
        }
        #endregion


        static private string getOffsetForGettingCellName(string strNumOrLetter, int numOffset, string strRowOrCol)
        {
            int numResult = 0;
            if (strRowOrCol == "row")
            {
                numResult = numOffset + Convert.ToInt32(strNumOrLetter) - 1;
                return numResult.ToString();
            }
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            if (strRowOrCol == "col")
            {
                if (strNumOrLetter.Length > 2)
                {
                    throw new Exception("more then two letters on the start column , not supported !!!");
                }
                int intGetNumberFromColLetter = 0;
                for (int idx = 0; idx < strNumOrLetter.Length; idx++)
                {
                    intGetNumberFromColLetter = Convert.ToChar(strNumOrLetter.Substring(idx, 1)) - 64;
                    if (strNumOrLetter.Length == 2 && idx == 0)
                    {
                        numResult += intGetNumberFromColLetter * 26;
                    }
                    else
                    {
                        numResult += intGetNumberFromColLetter + numOffset - 1;
                    }
                }
            }
            return numResult.ToString();
        }


        static private string getCellName(int row, int col)
        {
            if (col > 1566)
            {
                throw new Exception("more then two letters on the start column , not supported !!!");
            }

            string result;
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            int offset = 0;
            while (col > 26)
            {
                col -= 26;
                offset++;
            }
            result = "";
            if (offset > 0)
            {
                result += letters[offset - 1];
            }
            result += letters[col - 1];
            result += row.ToString();
            return (result);
        }






        #region ConvertToStringArray
        private string[] ConvertToStringArray(System.Array values, string rangeCol1,
                                         string rangeRow1, string rangeCol2, string rangeRow2)
        {
            string[] newArray = new string[values.Length];

            int index = 0;
            for (int i = values.GetLowerBound(0); i <= values.GetUpperBound(0); i++)
            {
                for (int j = values.GetLowerBound(1); j <= values.GetUpperBound(1); j++)
                {
                    if (values.GetValue(i, j) == null)
                    {
                        newArray[index] = " " + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    else
                    {
                        newArray[index] = values.GetValue(i, j).ToString() + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    index++;
                }
            }
            return newArray;
        }
        #endregion




    }
}
