using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Reflection;
using giyusBL;

namespace giyusConsole
{
    class clsLoadPd
    {
        public Excel.Workbook oWorkBook;
        public Excel.ApplicationClass oAppClass = null;
        public Excel.Worksheet oWorkSheet;

        public Excel.Range workingRangeCells = null;
        public Excel.Range A1MarginRange = null;

        public static string[] arrDelimiter = new string[1] { "!$!" };
        public string[] strMarginSplitter = new string[1] { ";" }; // between margins
        public string[] strRangeSplitter = new string[1] { "!" }; // between each range name and cells
        public string[] yaelSplitter = new string[1] { "!y@" };

        static string strData;
        static public string strWordInputDir = "";
        static string[] strDataSplitter = new string[1];
        static string[] strDataSplitter2 = new string[1];
        //static int lineCount = 2;

      static public string customer_id; //int
	  static public string customer_firstName;
      static public string customer_lastName;
      static public string customer_nid;
      static public string customer_degree;
      static public string customer_sex;
      static public string customer_startDate; // date,
      static public string customer_endDate; // date,
      static public string customer_street;
      static public string customer_houseNum;
      static public string customer_city;
      static public string customer_zip;
      static public string customer_phone;
      static public string customer_cell;
      static public string customer_unit;
      //static public string customer_headQuarters;
      static public string customer_birthDate; // date,
      static public string customer_bankBranch;
      static public string customer_bankBranchNum; //int
      static public string customer_bank;
      static public string customer_accountNo;
      //static public string customer_state;
      static public string customer_stateName;
      static public string customer_mateName;
      static public string customer_mateNid;
      static public string customer_father;
      static public string customer_fatherNid;
      static public string customer_mother;
      static public string customer_motherNid;
      //static public string customer_divisionCode;
    static public string customer_divisionName;
    static public Boolean customer_isOfficer;
    static public Boolean customer_isFighter;

    static public Boolean customer_isProf_notUsed;
    static public string customer_stateDate; //date
    static public string customer_childName;
    static public string customer_childNid;
    static public string customer_childBirthDate; //date
    static public string customer_childAgeNotUsed;

        static public string customer_type;

        
        Boolean blnWordVisible = Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);
        int pdLoadLineStart = Convert.ToInt32(ConfigurationManager.AppSettings["pdLoadLineStart"]);
        
        public void doWord()
        {
            int idx;
            try
            {
                Console.WriteLine("start pd=" + DateTime.Now);
                string msg = "����� ���� �������� ����";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg,2,1); //msgType - 2=info , 1=error

                strWordInputDir = Program.paramStrWordWorkDir;//ConfigurationManager.AppSettings["wordInputDir"];
                //            strDataSplitter[0] = ",";
                //            strDataSplitter2[0] = "!$!";
                //strConStr = ConfigurationManager.AppSettings["rafManPowerConnectionString"];
                // *******************  make connection ******************************************************
                GiyusDB.clsDbGlobal.makeConn();
                string[] arrMarginsRanges = null;
                    
                //string[] arrSubData = null;
                //string[] arrSubMarginsRanges;

                CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                oAppClass = new Excel.ApplicationClass();

                //******************************excel Alerts ****************************
                oAppClass.Visible = blnWordVisible;
                oAppClass.DisplayAlerts = blnWordVisible;

                #region excel
                oWorkBook = oAppClass.Workbooks.Open(strWordInputDir + "\\pd.xls",
                    Missing.Value, (object)false, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                //******************************excel Alerts ****************************
                // ******************************************************************//
                //get display_Names sheet
                oWorkSheet = (Excel.Worksheet)oWorkBook.Sheets.get_Item(1);
                //FindExcelWorksheetByName(oWorkBook, "Sheet1");
                if (oWorkSheet == null)
                {
                    msg = "���� ������ ������� ������ !";
                    Program.writeToTasksLogTableForRow(msg, 1, 1);
                    throw new Exception(msg);
                }

                Program.lineCount = pdLoadLineStart;
                idx = Program.lineCount;

                while (oWorkSheet.get_Range("A" + idx.ToString() + ":A" + idx.ToString(), Type.Missing).Value2 != null &&
                        oWorkSheet.get_Range("A" + idx.ToString() + ":A" + idx.ToString(), Type.Missing).Value2.ToString().Length > 5)
                {
                    idx++;
                }

                idx--;

                //A1MarginRange = oWorkSheet.get_Range("A1:A1", Type.Missing);
                Array arrayMargins = GetRangeAsArray(oWorkSheet,
                    "A" + Program.lineCount.ToString() + ":AK" + idx.ToString(), false);



                string prevCustomer_id = "0";

                for (int i = arrayMargins.GetLowerBound(0); i <= arrayMargins.GetUpperBound(0); i++)
                {
                    Program.lineCount++;
                    //collect all cells of every margin row and split them to work with array for each range
                    strData = "";
                    for (int j = arrayMargins.GetLowerBound(1); j <= arrayMargins.GetUpperBound(1); j++)
                    {
                        if (arrayMargins.GetValue(i, j) == null)
                        {
                            strData += "" + strMarginSplitter[0];
                        }
                        else
                        {
                            strData += arrayMargins.GetValue(i, j).ToString().Trim() + strMarginSplitter[0];
                        }
                    }
                    arrMarginsRanges = strData.Split(strMarginSplitter, StringSplitOptions.None);
                    //for (int idx = 0; idx < arrMarginsRanges.Length; idx++)
                    //{
                    //string strFileName = "";

                    customer_id = arrMarginsRanges[0]; //int
                    //************************************************ check the population letter A=checkup *************************************************************
                    if (customer_id.ToUpper().EndsWith("A"))
                    {
                        customer_type = Program.enumPopulation.checkUp.GetHashCode().ToString();
                    }
                    else
                    {
                        customer_type = Program.enumPopulation.reguler.GetHashCode().ToString();
                    }
                    customer_id = customer_id.ToUpper().TrimEnd('A');//remove suffix of type if any


                    if (customer_id != prevCustomer_id) //not first time, do save only if they are not equal
                    {
                        //make prev equal to current to save customer only once
                        prevCustomer_id = customer_id;

                        customer_firstName = arrMarginsRanges[1];
                        customer_lastName = arrMarginsRanges[2];
                        customer_nid = arrMarginsRanges[3];
                        customer_degree = arrMarginsRanges[4];
                        customer_sex = arrMarginsRanges[5];
                        customer_startDate = convertExcelDate(arrMarginsRanges[6]); // date,
                        customer_endDate = convertExcelDate(arrMarginsRanges[7]); // date,
                        customer_street = arrMarginsRanges[8];
                        customer_houseNum = arrMarginsRanges[9];
                        customer_city = arrMarginsRanges[10];
                        customer_zip = arrMarginsRanges[11];
                        customer_phone = convertExcelPhone(arrMarginsRanges[12]);
                        customer_cell = convertExcelPhone(arrMarginsRanges[13]);
                        customer_unit = Hebrew.reverseHebrewWordsOrderWhenLastIsNum(arrMarginsRanges[14]);
                        //customer_headQuarters = arrMarginsRanges[15];
                        customer_birthDate = convertExcelDate(arrMarginsRanges[15]); // date,
                        customer_bankBranch = arrMarginsRanges[16];
                        customer_bankBranchNum = arrMarginsRanges[17]; //int
                        customer_bank = arrMarginsRanges[18];
                        customer_accountNo = arrMarginsRanges[19];
                        //customer_state = arrMarginsRanges[20];

//1003	27	1	state	state	����
//1004	27	1	state	state	����
//1005	27	1	state	state	����
//1006	27	1	state	state	����
//1007	27	1	state	state	���� ������
                        switch (arrMarginsRanges[20].Trim())
                        {
                            case "����":
                                customer_stateName = "1003";
                                break;
                            case "����":
                                customer_stateName = "1004";
                                break;
                            case "����":
                                customer_stateName = "1005";
                                break;
                            case "����":
                                customer_stateName = "1006";
                                break;
                            case "���� ������":
                                customer_stateName = "1007";
                                break;
                            default:
                                customer_stateName = "1003";
                                break;
                        }
                        //customer_stateName = arrMarginsRanges[20].Trim();

                        customer_mateName = arrMarginsRanges[21];
                        if (arrMarginsRanges[22] == "000000000")
                        {
                            customer_mateNid = "null";
                        }
                        else
                        {
                            customer_mateNid = arrMarginsRanges[22];
                        }
                        customer_father = arrMarginsRanges[23];
                        customer_fatherNid = arrMarginsRanges[24];
                        customer_mother = arrMarginsRanges[25];
                        customer_motherNid = arrMarginsRanges[26];
                        //customer_divisionCode = arrMarginsRanges[27];
                        customer_divisionName = arrMarginsRanges[28];
                        customer_isOfficer = clsUtil.convert_str_notEmpty_ToBooleanTrue_elseBooleanFalse(arrMarginsRanges[29]);
                        customer_isFighter = clsUtil.convert_str_notEmpty_ToBooleanTrue_elseBooleanFalse(arrMarginsRanges[30]);
                        customer_isProf_notUsed = clsUtil.convert_str_notEmpty_ToBooleanTrue_elseBooleanFalse(arrMarginsRanges[31]);
                        customer_stateDate = convertExcelDate(arrMarginsRanges[32]);
                        save();
                    }

                    if (arrMarginsRanges[34].Trim() != "" && arrMarginsRanges[34].Trim() != "000000000")
                    {
                        customer_childName = arrMarginsRanges[33].Trim();
                        customer_childNid = arrMarginsRanges[34];
                        customer_childBirthDate = convertExcelDate(arrMarginsRanges[35]);
                        customer_childAgeNotUsed = arrMarginsRanges[36];
                        saveChild();
                    }
                    //if any other value is full, then raise error
                    else if (arrMarginsRanges[33].Trim() != "" || arrMarginsRanges[35].Trim() != "" || arrMarginsRanges[36].Trim() != "")
                    {
                        msg = string.Format("������� ��� ���� ���� {0} ���� ��� ��� �.�, ���� ��� ��� ��� - ��� �� �� �����", customer_id);
                        Program.writeToScreenAndToLog(msg, true);
                        Program.writeToTasksLogTableForRow(msg, 1, 1);
                    }
                }
                msg = "����� ����� ������� ������";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTableForRow(msg, 2, 1);



                #endregion excel

                #region close all excel
                try
                {
                    // ******************  close the excell documents *******************
                    object objSave = false;
                    object objTrue = true;
                    if (oWorkBook != null)
                    {
                        //close the first workBook
                        //oWorkBook.Close(objSave, (object)strFullFileName, Missing.Value);
                        oWorkBook.Close(objSave, Missing.Value, objTrue);
                        NAR(oWorkBook);
                        oWorkBook = null;
                        Process[] prc = Process.GetProcesses();
                        for (idx = 0; idx < prc.Length; idx++)
                        {
                            try
                            {
                                if (prc[idx].MainModule.ModuleName.StartsWith("EXCEL"))
                                {
                                    prc[idx].Kill();
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    // ******************  close the excell documents *******************

                }
                catch (Exception ex)
                {
                    msg = "����� ����� : \n"
                                    + " - ����� = " + ex.Message
                                    + "\n - ������ = " + ex.StackTrace;
                    string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message;

                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTableForRow(msg2, 1,1);

                }
                #endregion close all excel
            }
            catch (Exception ex)
            {
                string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message
                                + "\n - ������ = " + ex.StackTrace;
                string msg3 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg2, true);
                Program.writeToTasksLogTableForRow(msg3, 1, 1);

            }
            finally
            {
            }
        }

        private string convertExcelPhone(string strEvalPhone)
        {
            if (strEvalPhone == "000-0000000")
            {
                return "null";
            }
            else
            {
                return strEvalPhone;
            }
        }

        private string convertExcelDate(string strEvalDate)
        {
            string strFinalEvalDate = "";
            string msg = "";
            if (strEvalDate.Length > 10 || strEvalDate.Length < 6)
            {
                msg = "�����: " + " ���� ����� �� ���� - " + strEvalDate + "  -  ���� ����:" + customer_id;
                throw new Exception(msg);
            }
            else //length is 8-10
            {
                if (strEvalDate == "00/00/00")
                {
                    return "null";
                }
                int intFirstSep = 0; // find place of first seperator
                int intSeconedSep = 0; // find place of seconed seperator
                char charSep = '/';  //***** char sep searched, if point will be found,

                string strYearPrefix = "";
                //************************** then this char will be replaced to point
                char charSepResult = '/'; //the sep we will get in result even if the sep is '.'

                intFirstSep = strEvalDate.IndexOf('/');
                if (intFirstSep == -1)
                {
                    intFirstSep = strEvalDate.IndexOf('.');
                    charSep = '.'; //***** change char sep because we find point ******
                }
                if (intFirstSep == -1)
                {
                    msg = "�����: " + " ���� ����� �� ���� - " + strEvalDate + "  -  ���� ����:" + customer_id;
                    throw new Exception(msg);
                }
                else
                {
                    intSeconedSep = strEvalDate.IndexOf(charSep, intFirstSep + 1);
                    if (intSeconedSep == -1)
                    {
                        msg = "�����: " + " ���� ����� �� ���� - " + strEvalDate + "  -  ���� ����:" + customer_id;
                        throw new Exception(msg);
                    }
                }
                //find the year length is it 2 or 4 digits.....
                //by sub of length in intSecondSep
                int intYearLength = strEvalDate.Length - (intSeconedSep + 1);
                if (intYearLength == 4)
                {
                    strYearPrefix = "";
                }
                else if(intYearLength == 2)
                {
                    int myYear=Convert.ToInt32(strEvalDate.Substring(strEvalDate.Length-intYearLength));
                    if (myYear < 50)
                    {
                        strYearPrefix = "20";
                    }
                    else
                    {
                        strYearPrefix = "19";
                    }
                }
                strFinalEvalDate =
                    strEvalDate.Substring(0, intFirstSep).PadLeft(2, '0') + //day
                    charSepResult + strEvalDate.Substring(intFirstSep + 1, intSeconedSep - intFirstSep - 1).PadLeft(2, '0') +//month
                    charSepResult + strYearPrefix +  //strPrefix added in case of 2 digits year
                    strEvalDate.Substring(intSeconedSep + 1, intYearLength);

                //*****  if we got here then first and seconed sep are found  *****
                if (Convert.ToInt16(strFinalEvalDate.Substring(0, 2)) > 31 ||
                    Convert.ToInt16(strFinalEvalDate.Substring(3, 2)) > 12)
                {
                    msg = "�����: " + " ���� ����� �� ���� - " + strEvalDate + "  -  ���� ����:" + customer_id;
                    throw new Exception(msg);
                }
            }
            // ****************  date format is good ***************************

            //DateTime dtResult = DateTime.MinValue;
            //try
            //{
            //    //dtResult = Convert.ToDateTime( DateTime.FromOADate(Convert.ToDouble(strExcelDate));

            //    string format = "dd/mm/yyyy";
            //    Boolean blnSuccess = DateTime.TryParseExact(strFinalEvalDate, format,
            //            CultureInfo.InstalledUICulture, DateTimeStyles.AssumeLocal, out dtResult);
            //    if (!blnSuccess)
            //    {
            //        msg = "���� �� ��� ����� ����� ����: " + customer_id;
            //        throw new Exception(msg);
            //    }
            //    else
            //    {
            //    }
            //}
            //catch (Exception e)
            //{
            //    msg = "���� �� ��� ����� ����� ����: " + customer_id;
            //    throw new Exception(msg);
            //}
           // return dtResult.ToString("dd/MM/yyyy");
            return strFinalEvalDate;
        }

        private void saveChild()
        {

            string strInParamsNames = "customer_id,customer_childName,customer_childNid," +
                                        "customer_childBirthDate";

            
            string strInParamsValues = customer_id + arrDelimiter[0] + //int
                                        customer_childName + " " + customer_lastName + arrDelimiter[0] +
                                        customer_childNid + arrDelimiter[0] +
                                        customer_childBirthDate;

            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.None);

            string strInsertUpdate = "";
            try
            {
                strInsertUpdate = GiyusDB.clsDbGlobal.runSp_ScalarQuery_NoReturn("[giyus].insertNewCustomerChild", arrInParamsNames, arrInParamsValues);
                if (strInsertUpdate.Length > 10)
                {
                    string msg = "����� ������ ����� ������� : \n" +
                            strInsertUpdate;
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTableForRow(msg, 1, 1);
                }
                Console.WriteLine("end save customerChild=" + DateTime.Now);
            }
            catch (Exception ex)
            {
                string msg = "����� ����� : \n"
                + " - ����� = " + ex.Message + "\n"
                + " - ������ = " + ex.StackTrace;
                string msg2 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg, true);
                Program.writeToTasksLogTableForRow(msg2, 1, 1);
            }
        }

        private void save()
        {
            string strInParamsNames = "customer_id,customer_firstName,customer_lastName,customer_nid,customer_degree" +
                                      ",customer_sex,customer_startDate,customer_endDate,customer_street" +
                                      ",customer_houseNum,customer_city,customer_zip" +
                                      ",customer_phone,customer_cell,customer_unit" + //,customer_headQuarters" +
                                      ",customer_birthDate,customer_bankBranch,customer_bankBranchNum" +
                                      ",customer_bank,customer_accountNo" +
                //customer_state,
                                      ",customer_stateName,customer_mateName,customer_mateNid" +
                                      ",customer_father,customer_fatherNid,customer_mother,customer_motherNid" +
                                      //",customer_divisionCode
                                      ",customer_divisionName,customer_isOfficer,customer_isFighter" +
                                      ",customer_stateDate,customer_type";
            string strInParamsValues = customer_id + arrDelimiter[0] + //int
                                        customer_firstName + arrDelimiter[0] +
                                        customer_lastName + arrDelimiter[0] +
                                        customer_nid + arrDelimiter[0] +
                                        customer_degree + arrDelimiter[0] +
                                        customer_sex + arrDelimiter[0] +
                                        customer_startDate + arrDelimiter[0] + // date,
                                        customer_endDate + arrDelimiter[0] + // date,
                                        customer_street + arrDelimiter[0] +
                                        customer_houseNum + arrDelimiter[0] +
                                        customer_city + arrDelimiter[0] +
                                        customer_zip + arrDelimiter[0] +
                                        customer_phone + arrDelimiter[0] +
                                        customer_cell + arrDelimiter[0] +
                                        customer_unit + arrDelimiter[0] +
                //customer_headQuarters + arrDelimiter[0] +
                                        customer_birthDate + arrDelimiter[0] + // date,
                                        customer_bankBranch + arrDelimiter[0] +
                                        customer_bankBranchNum + arrDelimiter[0] + //int
                                        customer_bank + arrDelimiter[0] +
                                        customer_accountNo + arrDelimiter[0] +
                //customer_state + arrDelimiter[0] +
                                        customer_stateName + arrDelimiter[0] +
                                        customer_mateName + arrDelimiter[0] +
                                        customer_mateNid + arrDelimiter[0] +
                                        customer_father + arrDelimiter[0] +
                                        customer_fatherNid + arrDelimiter[0] +
                                        customer_mother + arrDelimiter[0] +
                                        customer_motherNid + arrDelimiter[0] +
                //customer_divisionCode + arrDelimiter[0] +
                                        customer_divisionName + arrDelimiter[0] +
                                        customer_isOfficer + arrDelimiter[0] +
                                        customer_isFighter + arrDelimiter[0] +
                                        customer_stateDate + arrDelimiter[0] +
                                        customer_type;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.None);

            string strInsertUpdate = "";
            try
            {
                strInsertUpdate = GiyusDB.clsDbGlobal.runSp_ScalarQuery_NoReturn("[giyus].insertNewCustomer", arrInParamsNames, arrInParamsValues);
                if (strInsertUpdate.Length >10)
                {
                    string msg = "����� ������ ����� ������� : \n" +
                            strInsertUpdate;
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTableForRow(msg, 1, 1);
                }
                Console.WriteLine("end save pd and start save qflow if enabled useQflow=" + DateTime.Now);

                if (Convert.ToBoolean(Program.useQflow))
                {

                    //*************************************   save customer in qflow   ****************************
                    strInParamsNames = "PersonalId,FirstName,LastName,customer_type";
                    strInParamsValues = customer_id + arrDelimiter[0] +
                                                customer_firstName + arrDelimiter[0] +
                                                customer_lastName + arrDelimiter[0] +
                                                customer_type;

                    arrInParamsNames = strInParamsNames.Split(',');
                    arrInParamsValues = strInParamsValues.Split(arrDelimiter, StringSplitOptions.None);

                    strInsertUpdate = "";
                    strInsertUpdate = GiyusDB.clsQflowDbGlobal.runSp_ScalarQuery_NoReturn
                        ("[giyus].[insertNewQFlowCustomer]", arrInParamsNames, arrInParamsValues);
                    if (strInsertUpdate.Length > 10)
                    {
                        string msg = "����� ������ ����� ������� : \n" +
                                strInsertUpdate;
                        Program.writeToScreenAndToLog(msg, true);
                        Program.writeToTasksLogTableForRow(msg, 1, 1);
                    }

                    Console.WriteLine("end save qflow and start save word Docs=" + DateTime.Now + " - if enabled makeDocs");
                }
            //****************************************  fill word documents **********************************
                if (Convert.ToBoolean(Program.makeDocs))
                {
                    clsWord clsLoad = new clsWord();
                    clsLoad.doWord();
                    Console.WriteLine("end save word Docs=" + DateTime.Now + "\n\n\n");
                }
            }
            catch (Exception ex)
            {
                string msg = "����� ����� : \n"
                + " - ����� = " + ex.Message + "\n"
                + " - ������ = " + ex.StackTrace;
                string msg2 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg, true);
                Program.writeToTasksLogTableForRow(msg2, 1, 1);
            }

        }


        //put this procedure in your code.

        static private void NAR(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }




        #region FIND EXCEL WORKSHEET by workSheet name
        static public Excel.Worksheet FindExcelWorksheetByName(Excel.Workbook wk, string worksheetName)
        {
            Excel.Worksheet tempWorkSheet;
            if (wk.Sheets != null)
            {
                for (int i = 1; i <= wk.Sheets.Count; i++)
                {
                    tempWorkSheet = (Excel.Worksheet)wk.Sheets.get_Item(i);
                    if (tempWorkSheet.Name.Equals(worksheetName))
                    {
                        return tempWorkSheet;
                    }
                }
            }
            return null;
        }
        #endregion

        #region GET RANGE

        public string[] GetRange(Excel.Worksheet excelSheet, string rangeCol1,
                                string rangeRow1, string rangeCol2, string rangeRow2)
        {
            workingRangeCells = excelSheet.get_Range(rangeCol1 + rangeRow1 + ":" +
                                                                rangeCol2 + rangeRow2, Type.Missing);
            //workingRangeCells.Select();
            System.Array array = (System.Array)workingRangeCells.Cells.Value2;
            string[] arrayValues = ConvertToStringArray(array, rangeCol1, rangeRow1, rangeCol2, rangeRow2);
            return arrayValues;
        }
        #endregion


        #region GetRange value2 or formula  AsArray

        public Array GetRangeAsArray(Excel.Worksheet excelSheet, string rangeString, Boolean blnGetFormula)
        {

            workingRangeCells = excelSheet.get_Range(rangeString, Type.Missing);
            //workingRangeCells.Select();
            if (blnGetFormula)
            {
                return (System.Array)workingRangeCells.Cells.Formula;
            }
            else
            {
                return (System.Array)workingRangeCells.Cells.Value2;
            }
        }
        #endregion


        static private string getOffsetForGettingCellName(string strNumOrLetter, int numOffset, string strRowOrCol)
        {
            int numResult = 0;
            if (strRowOrCol == "row")
            {
                numResult = numOffset + Convert.ToInt32(strNumOrLetter) - 1;
                return numResult.ToString();
            }
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            if (strRowOrCol == "col")
            {
                if (strNumOrLetter.Length > 2)
                {
                    throw new Exception("more then two letters on the start column , not supported !!!");
                }
                int intGetNumberFromColLetter = 0;
                for (int idx = 0; idx < strNumOrLetter.Length; idx++)
                {
                    intGetNumberFromColLetter = Convert.ToChar(strNumOrLetter.Substring(idx, 1)) - 64;
                    if (strNumOrLetter.Length == 2 && idx == 0)
                    {
                        numResult += intGetNumberFromColLetter * 26;
                    }
                    else
                    {
                        numResult += intGetNumberFromColLetter + numOffset - 1;
                    }
                }
            }
            return numResult.ToString();
        }


        static private string getCellName(int row, int col)
        {
            if (col > 1566)
            {
                throw new Exception("more then two letters on the start column , not supported !!!");
            }

            string result;
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            int offset = 0;
            while (col > 26)
            {
                col -= 26;
                offset++;
            }
            result = "";
            if (offset > 0)
            {
                result += letters[offset - 1];
            }
            result += letters[col - 1];
            result += row.ToString();
            return (result);
        }






        #region ConvertToStringArray
        private string[] ConvertToStringArray(System.Array values, string rangeCol1,
                                         string rangeRow1, string rangeCol2, string rangeRow2)
        {
            string[] newArray = new string[values.Length];

            int index = 0;
            for (int i = values.GetLowerBound(0); i <= values.GetUpperBound(0); i++)
            {
                for (int j = values.GetLowerBound(1); j <= values.GetUpperBound(1); j++)
                {
                    if (values.GetValue(i, j) == null)
                    {
                        newArray[index] = " " + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    else
                    {
                        newArray[index] = values.GetValue(i, j).ToString() + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    index++;
                }
            }
            return newArray;
        }
        #endregion




    }
}
