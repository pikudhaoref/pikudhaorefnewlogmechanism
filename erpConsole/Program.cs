using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Reflection;

namespace giyusConsole
{
    class Program
    {

        public enum enumPopulation
        {
            reguler = 1,
            checkUp = 2
        }

        static string sSource = "giyusLoadData";
        static string[] strDataSplitter = new string[1];
        static string[] strDataSplitter2 = new string[1];
        public static int lineCount = 0;
        static public Boolean blnFullLog = Convert.ToBoolean(ConfigurationManager.AppSettings["fullLog"]);
        

        static public DataTable dtTasks = null;

        static public string paramStrPass = "";
        static public int intCubicHeight = 0;
        static public string paramStrWordWorkDir = "";
        static public string paramStrPptWorkDir = "";
        static public string paramStrOutputDir = "";
        static public string StrPptDownFileResult = "";



        static public Excel.Workbook oWorkBook;
        static public Excel.ApplicationClass oAppClass = null;
        static public Excel.Worksheet oWorkSheet;

        static public Excel.Range workingRangeCells = null;
        static public Excel.Range A1MarginRange = null;

        static public string[] strMarginSplitter = new string[1] { ";" }; // between margins
        static public string[] strRangeSplitter = new string[1] { "!" }; // between each range name and cells
        static public string[] yaelSplitter = new string[1] { "!y@" };

        static public string[] arrMarginsRanges = null;
        static public string msg = "";
        static public string strNewFileName = "";

        static public string useQflow = ConfigurationManager.AppSettings["useQflow"];
        static public string makeDocs = ConfigurationManager.AppSettings["makeDocs"];
        static public string firstDoc = ConfigurationManager.AppSettings["firstDoc"];

        static void Main(string[] args)
        {
            strDataSplitter[0] = ",";
            strDataSplitter2[0] = "!$!";
            string strFileResult = "";
            try
            {
                if (args.Length > 0)
                {
                    if (args[0] == "evt")
                    {
                        string sSource = "pikudLoadData";
                        string sLog = "pikudLoadData";
                        if (!EventLog.SourceExists(sSource))
                        {
                            EventLog.CreateEventSource(sSource, sLog);
                        }

                        sSource = "pikudDB";
                        sLog = "pikudDB";
                        if (!EventLog.SourceExists(sSource))
                        {
                            EventLog.CreateEventSource(sSource, sLog);
                        }

                        sSource = "pikudGui";
                        sLog = "pikudGui";
                        if (!EventLog.SourceExists(sSource))
                        {
                            EventLog.CreateEventSource(sSource, sLog);
                        }

                        sSource = "pikudBL";
                        sLog = "pikudBL";
                        if (!EventLog.SourceExists(sSource))
                        {
                            EventLog.CreateEventSource(sSource, sLog);
                        }
                        Console.WriteLine("log created, press any key");
                        Console.ReadLine();
                        return;
                    }
                    else if (args[0] == "enc")
                    {
                        EncryptConfigSection(args[1]);
                        return;
                    }
                }
                GiyusDB.clsDbGlobal.makeConn();
                if (Convert.ToBoolean(Program.useQflow))
                {
                    GiyusDB.clsQflowDbGlobal.makeConn();
                }
                dtTasks = GiyusDB.clsDbGlobal.GetDtFromSPNoParamsNoReturnValue("[giyus].getTaskOrders_Waiting");
                string[] arrData = null;

                if (dtTasks == null || dtTasks.Rows.Count == 0)
                {
                    //string msg2 = "��� ������ ������";
                    //Program.writeToScreenAndToLog(msg2, false);
                    //Program.writeToTasksLogTable(msg2,2, 5);
                    return;
                }
                else
                {
                    closeAllExcelWordPpt();
                    //get parameters
                    DataTable dtParams = GiyusDB.clsDbGlobal.getParamsByTableNum(0, 24);
//                    paramStrPass = dtParams.Rows[0]["param_value"].ToString();
                    paramStrWordWorkDir = dtParams.Rows[1]["param_value"].ToString();
                    paramStrPptWorkDir = dtParams.Rows[2]["param_value"].ToString();
                    paramStrOutputDir = dtParams.Rows[3]["param_value"].ToString();
                    arrData = dtTasks.Rows[0]["taskOrder_Data"].ToString().Split(' ');
                    if (arrData[0] == "loadPd")
                    {
                        clsLoadPd loadPd = new clsLoadPd();
                        loadPd.doWord();
                    }
                    //if (arrData[0] == "loadFirstWord")
                    //{
                    //    clsWord clsLoad = new clsWord();
                    //    clsWord.customerID = arrData[1];
                    //    clsWord.strWordInputDir = Program.paramStrWordWorkDir;
                    //    clsWord.strWordOutputDir = Program.paramStrOutputDir;
                    //    clsWord.strFileName = firstDoc;
                    //    clsWordLoad myWordLoad = new clsWordLoad();
                    //    myWordLoad.loadDoc();
                    //    Console.WriteLine("end save first word Doc=" + DateTime.Now + "\n\n\n");
                    //}
                    else if (arrData[0] == "loadWord")
                    {
                        clsWord clsLoad = new clsWord();
                        clsWord.customerID = arrData[1];
                        clsLoad.doWord();
                        Console.WriteLine("end save word Docs=" + DateTime.Now + "\n\n\n");
                    }
                    else if (arrData[0] == "rptCompany")
                    {
                        clsRptCompany rept = new clsRptCompany();
                        rept.doReport(arrData);
                        Console.WriteLine("end company report rptCompany=" + DateTime.Now + "\n\n\n");
                    }
                    else if (arrData[0] == "rptStat")
                    {
                        clsRptStat rept = new clsRptStat();
                        rept.doReport(arrData);
                        Console.WriteLine("end stat report rptStat=" + DateTime.Now + "\n\n\n");
                    }
                    else if (arrData[0] == "rptSoldier")
                    {
                        clsRptSoldier rept = new clsRptSoldier();
                        rept.doReport(arrData);
                        Console.WriteLine("end stat report rptSoldier=" + DateTime.Now + "\n\n\n");
                    }
                    updateTaskOrders(dtTasks.Rows[0]["taskOrder_id"].ToString(),
                                                strNewFileName, "3", "", "");
                }
            }
            catch (Exception ex)
            {
                string err = "msg=" + ex.Message + "\n stack=" + ex.StackTrace;
                string msg2 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                writeToScreenAndToLog(err, true);
                writeToTasksLogTable(msg2, 1, 5);
                updateTaskOrders(dtTasks.Rows[0]["taskOrder_id"].ToString(),
                                                    strFileResult, "4", msg2,"");
                return;
            }
        }

        private static void closeAllExcelWordPpt()
        {
            Process[] prc = Process.GetProcesses();
            for (int idx = 0; idx < prc.Length; idx++)
            {
                try
                {
                    if (prc[idx].MainModule.ModuleName.ToUpper().StartsWith("EXCEL"))
                    {
                        prc[idx].Kill();
                    }
                    if (prc[idx].MainModule.ModuleName.ToUpper().StartsWith("POWERPNT"))
                    {
                        prc[idx].Kill();
                    }
                    if (prc[idx].MainModule.ModuleName.ToUpper().StartsWith("WINWORD"))
                    {
                        prc[idx].Kill();
                    }
                }
                catch
                {
                }
            }
        }

        public static void updateTaskOrders(string id,string fileResult, 
                                            string state, string error,string excelLog)
        {
            string strInParamsNames = "id" + strDataSplitter2[0] +
                                        "fileResult" + strDataSplitter2[0] +
                                        "state" + strDataSplitter2[0] +
                                        "error" + strDataSplitter2[0] +
                                        "excelLog";
            string strInParamsValues = id + strDataSplitter2[0] +
                                       fileResult + strDataSplitter2[0] +
                                       state + strDataSplitter2[0] +
                                       error + strDataSplitter2[0] +
                                       excelLog;

            string[] arrInParamsNames = strInParamsNames.Split(strDataSplitter2, StringSplitOptions.None);
            string[] arrInParamsValues = strInParamsValues.Split(strDataSplitter2, StringSplitOptions.None);
            if (!giyusBL.clsUtil.ExecuteSQL("giyus.updateTaskOrders", arrInParamsNames, arrInParamsValues))
            {
                
            }
        }




        static private void EncryptConfigSection(string strPath)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(strPath);
            ConfigurationSection section1 = config.AppSettings;
            ConfigurationSection section2 = config.ConnectionStrings;
            if (section1 != null)
            {
                if (!section1.SectionInformation.IsProtected)
                {
                    if (!section1.ElementInformation.IsLocked)
                    {
                        section1.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                        section1.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
                else
                {
                    if (!section1.ElementInformation.IsLocked)
                    {
                        section1.SectionInformation.UnprotectSection();
                        section1.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }

                }
            }
            if (section2 != null)
            {
                if (!section2.SectionInformation.IsProtected)
                {
                    if (!section2.ElementInformation.IsLocked)
                    {
                        section2.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                        section2.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
                else
                {
                    if (!section2.ElementInformation.IsLocked)
                    {
                        section2.SectionInformation.UnprotectSection();
                        section2.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }

                }
            }
        }



        static private void makeExcelLog(DataTable dt, string strOrigFileName)
        {
            try
            {
                CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                oAppClass = new Excel.ApplicationClass();

                //******************************excel Alerts ****************************
                oAppClass.Visible = false;
                oAppClass.DisplayAlerts = false;

                #region excel
                oWorkBook = oAppClass.Workbooks.Open(strOrigFileName,
                    Missing.Value, (object)false, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                //******************************excel Alerts ****************************
                // ******************************************************************//
                //get display_Names sheet
                oWorkSheet = FindExcelWorksheetByName(oWorkBook, "Sheet1");
                if (oWorkSheet == null)
                {
                    msg = @"������ ��� - """ + "Sheet1" + @""" �� ����";

                    throw new Exception(msg);
                }
                string let = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int idxLetter = 1; idxLetter <= 5; idxLetter++)
                    {
                        switch (idxLetter)
                        {
                            case 1:
                                let = "A";
                                break;
                            case 2:
                                let = "B";
                                break;
                            case 3:
                                let = "C";
                                break;
                            case 4:
                                let = "D";
                                break;
                            case 5:
                                let = "E";
                                break;
                        }
                        A1MarginRange = oWorkSheet.get_Range(let + (i+2).ToString() + ":" + let + (i+2).ToString(),Type.Missing);
                        A1MarginRange.Value2 = dt.Rows[i][idxLetter - 1].ToString();
                        //Array arrayMargins = GetRangeAsArray(oWorkSheet, A1MarginRange.Value2.ToString(), false);
                    }
                }

                #endregion excel
                #region close all excel
                try
                {
                    //strNewFileName =  Guid.NewGuid().ToString() + ".xlsx";
                    //oWorkBook.SaveAs(Program.paramStrOutputDir + "\\errorLogs\\" + strNewFileName,
                    //    Missing.Value, Missing.Value,
                    //    Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlExclusive,
                    //    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                    // ******************  close the excell documents *******************
                    object objSave = false;
                    object objTrue = true;
                    if (oWorkBook != null)
                    {
                        //close the first workBook
                        //oWorkBook.Close(objSave, (object)strFullFileName, Missing.Value);
                        oWorkBook.Close(objSave, Missing.Value, objTrue);
                        NAR(oWorkBook);
                        oWorkBook = null;
                        Process[] prc = Process.GetProcesses();
                        for (int idx = 0; idx < prc.Length; idx++)
                        {
                            try
                            {
                                if (prc[idx].MainModule.ModuleName.StartsWith("EXCEL"))
                                {
                                    prc[idx].Kill();
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    // ******************  close the excell documents *******************
                }
                catch (Exception ex)
                {
                    msg = "����� ����� : \n"
                                    + " - ����� = " + ex.Message + "\n"
                                    + " - ������ = " + ex.StackTrace;
                    string msg2 = "����� ����� : \n"
                                + " - ����� = " + ex.Message;
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTable(msg2,1, 5);
                }
                #endregion close all excel
            }
            catch (Exception ex)
            {
                msg = "����� ����� : \n"
                                + " - ����� = " + ex.Message + "\n"
                                + " - ������ = " + ex.StackTrace;
                string msg2 = "����� ����� : \n"
                            + " - ����� = " + ex.Message;
                Program.writeToScreenAndToLog(msg, true);
                Program.writeToTasksLogTable(msg2, 1, 5);
            }
        }



        //put this procedure in your code.

        static private void NAR(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }



        #region FIND EXCEL WORKSHEET by workSheet name
        static public Excel.Worksheet FindExcelWorksheetByName(Excel.Workbook wk, string worksheetName)
        {
            Excel.Worksheet tempWorkSheet;
            if (wk.Sheets != null)
            {
                for (int i = 1; i <= wk.Sheets.Count; i++)
                {
                    tempWorkSheet = (Excel.Worksheet)wk.Sheets.get_Item(i);
                    if (tempWorkSheet.Name.Equals(worksheetName))
                    {
                        return tempWorkSheet;
                    }
                }
            }
            return null;
        }
        #endregion

        #region GET RANGE

        public string[] GetRange(Excel.Worksheet excelSheet, string rangeCol1,
                                string rangeRow1, string rangeCol2, string rangeRow2)
        {
            workingRangeCells = excelSheet.get_Range(rangeCol1 + rangeRow1 + ":" +
                                                                rangeCol2 + rangeRow2, Type.Missing);
            //workingRangeCells.Select();
            System.Array array = (System.Array)workingRangeCells.Cells.Value2;
            string[] arrayValues = ConvertToStringArray(array, rangeCol1, rangeRow1, rangeCol2, rangeRow2);
            return arrayValues;
        }
        #endregion


        #region GetRange value2 or formula  AsArray

        public Array GetRangeAsArray(Excel.Worksheet excelSheet, string rangeString, Boolean blnGetFormula)
        {

            workingRangeCells = excelSheet.get_Range(rangeString, Type.Missing);
            //workingRangeCells.Select();
            if (blnGetFormula)
            {
                return (System.Array)workingRangeCells.Cells.Formula;
            }
            else
            {
                return (System.Array)workingRangeCells.Cells.Value2;
            }
        }
        #endregion


        static private string getOffsetForGettingCellName(string strNumOrLetter, int numOffset, string strRowOrCol)
        {
            int numResult = 0;
            if (strRowOrCol == "row")
            {
                numResult = numOffset + Convert.ToInt32(strNumOrLetter) - 1;
                return numResult.ToString();
            }
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            if (strRowOrCol == "col")
            {
                if (strNumOrLetter.Length > 2)
                {
                    throw new Exception("more then two letters on the start column , not supported !!!");
                }
                int intGetNumberFromColLetter = 0;
                for (int idx = 0; idx < strNumOrLetter.Length; idx++)
                {
                    intGetNumberFromColLetter = Convert.ToChar(strNumOrLetter.Substring(idx, 1)) - 64;
                    if (strNumOrLetter.Length == 2 && idx == 0)
                    {
                        numResult += intGetNumberFromColLetter * 26;
                    }
                    else
                    {
                        numResult += intGetNumberFromColLetter + numOffset - 1;
                    }
                }
            }
            return numResult.ToString();
        }


        static private string getCellName(int row, int col)
        {
            if (col > 1566)
            {
                throw new Exception("more then two letters on the start column , not supported !!!");
            }

            string result;
            char[] letters = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
            int offset = 0;
            while (col > 26)
            {
                col -= 26;
                offset++;
            }
            result = "";
            if (offset > 0)
            {
                result += letters[offset - 1];
            }
            result += letters[col - 1];
            result += row.ToString();
            return (result);
        }






        #region ConvertToStringArray
        private string[] ConvertToStringArray(System.Array values, string rangeCol1,
                                         string rangeRow1, string rangeCol2, string rangeRow2)
        {
            string[] newArray = new string[values.Length];

            int index = 0;
            for (int i = values.GetLowerBound(0); i <= values.GetUpperBound(0); i++)
            {
                for (int j = values.GetLowerBound(1); j <= values.GetUpperBound(1); j++)
                {
                    if (values.GetValue(i, j) == null)
                    {
                        newArray[index] = " " + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    else
                    {
                        newArray[index] = values.GetValue(i, j).ToString() + yaelSplitter[0] + getOffsetForGettingCellName(rangeCol1, j, "col") + strRangeSplitter[0] + getOffsetForGettingCellName(rangeRow1, i, "row");
                    }
                    index++;
                }
            }
            return newArray;
        }
        #endregion














 
        /// <summary>
        /// isErr: 1=error 2=info
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="isErr"></param>
        public static void writeToTasksLogTable(string msg,int intMsgType,int intTaskType)
        {
            string strFullMsg = "���� ���� ����� : " + lineCount + "\n\n    -    " + msg;
            string strInParamsNames = "taskLog_taskOrderID" + strDataSplitter2[0] +
                                        "taskLog_msgType" + strDataSplitter2[0] +
                                        "taskLog_taskType" + strDataSplitter2[0] +
                                        "taskLog_msg" + strDataSplitter2[0] +
                                        "taskLog_file";
            string strInParamsValues = dtTasks.Rows[0]["taskOrder_id"].ToString() + strDataSplitter2[0] +
                                       intMsgType.ToString() + strDataSplitter2[0] +
                                       intTaskType + strDataSplitter2[0] +
                                       strFullMsg + strDataSplitter2[0];
                                       //clsWord.strFileName;

            string[] arrInParamsNames = strInParamsNames.Split(strDataSplitter2, StringSplitOptions.None);
            string[] arrInParamsValues = strInParamsValues.Split(strDataSplitter2, StringSplitOptions.None);
            if (!GiyusDB.clsDbGlobal.runSp_NoReturn("[giyus].insertTaskLog", arrInParamsNames, arrInParamsValues))
            {

            }
        }


        public static void writeToTasksLogTableForRow(string msg, int intMsgType, int intTaskType)
        {
            string strFullMsg = "";
            if (lineCount == 0)
            {
                strFullMsg = msg;
            }
            else
            {
                strFullMsg = "���� ���� �����  : " + lineCount + "\n\n    -    " + msg;
            }
            string strInParamsNames = "taskLog_taskOrderID" + strDataSplitter2[0] +
                                        "taskLog_msgType" + strDataSplitter2[0] +
                                        "taskLog_taskType" + strDataSplitter2[0] +
                                        "taskLog_msg" + strDataSplitter2[0] +
                                        "taskLog_file";
            string strInParamsValues = dtTasks.Rows[0]["taskOrder_id"].ToString() + strDataSplitter2[0] +
                                       intMsgType.ToString() + strDataSplitter2[0] +
                                       intTaskType + strDataSplitter2[0] +
                                       strFullMsg + strDataSplitter2[0];
                                       //clsWord.strFileName;

            string[] arrInParamsNames = strInParamsNames.Split(strDataSplitter2, StringSplitOptions.None);
            string[] arrInParamsValues = strInParamsValues.Split(strDataSplitter2, StringSplitOptions.None);
            if (!GiyusDB.clsDbGlobal.runSp_NoReturn("[giyus].insertTaskLog", arrInParamsNames, arrInParamsValues))
            {

            }
        }


  

        public static void writeToScreenAndToLog(string msg,Boolean isErr)
        {
            //if (!EventLog.SourceExists(sSource))
            //    EventLog.CreateEventSource(sSource, sLog);

            //if (blnPrintToConsole)
            //{
            //    Console.WriteLine(isErr.ToString() + "    -    " +  msg);
            //}
            if (isErr)
            {
                EventLog.WriteEntry(sSource, " the error found on line number: " + lineCount + "\n\n" + msg
                                            , EventLogEntryType.Error, 1);
            }
            else
            {
                EventLog.WriteEntry(sSource, " the msg is for line number: " + lineCount + "\n\n" + msg
                                            , EventLogEntryType.Information, 2);
            }
        }
    }
}
