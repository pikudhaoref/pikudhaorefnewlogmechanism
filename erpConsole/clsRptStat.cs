using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using msWord = Microsoft.Office.Interop.Word;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.IO;
using giyusBL;

namespace giyusConsole
{
    class clsRptStat
    {


        Microsoft.Office.Interop.Word.Application varWord;
        Microsoft.Office.Interop.Word.Document varDoc;
        //Microsoft.Office.Interop.Word.Range varRange;
        //Microsoft.Office.Interop.Word.Bookmark varBookMark;

        public static string[] arrDelimiter = new string[1] { "!$!" };

        object strNewFile = "";

        object objUnitWdChar = msWord.WdUnits.wdCharacter;
        object objUnitWdLine = msWord.WdUnits.wdLine;
        object objUnitWord = msWord.WdUnits.wdWord;
        object objUnitWdTable = msWord.WdUnits.wdTable;
        object objUnitWdStory = msWord.WdUnits.wdStory;
        object objUnitCell = msWord.WdUnits.wdCell;
        object objUnitRow = msWord.WdUnits.wdRow;
        object objUnitPar = msWord.WdUnits.wdParagraph;
        object objFalse = false;
        object objTrue = true;
        object objMissing = Type.Missing;
        //msWord.TextInput wordInput;

        object objCountOne = 1;
        object objCountTwo = 2;
        object objCountThree = 3;
        object objCountFour = 4;
        //int intGradeCounter = 0;
        //int intProp2RowsCount = 0;

        Boolean blnWordVisible = Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);
        Boolean blnWaitForKey = Convert.ToBoolean(ConfigurationManager.AppSettings["waitForKey"]);
        object objPass = null;
        string msg = "";


        public void TypeTextBookMarkAndLeaveInputField(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            if (!varDoc.Bookmarks.Exists(bkName))
            {
                return;
            }
            //varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            //varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToField, objMissing, objMissing, bkName).Select();
            varDoc.FormFields.get_Item(bkName).TextInput.Default = customerValue;
            //varDoc.FormFields.set_Item(bkName).Select();


            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //varWord.Selection.MoveLeft(msWord.WdUnits.wdCharacter, 1, objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }



        public void addBookMark(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, bkName);
            varWord.Selection.TypeText("abc");
            //varDoc.FormFields.get_Item(1).Range.Select();
            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }
        protected string cleanString(string myStr)
        {
            return myStr.Replace("\r\a", "").Trim().Replace("\r", " ").ToLower();
        }

        public void fillBookMark(string strFieldName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            //string strBookMark = "";
            //for (int idxCounter = 1; idxCounter <= 3; idxCounter++)
            //{
            //strBookMark = strFieldName; // +idxCounter.ToString();
            if (varDoc.Bookmarks.Exists(strFieldName))
                {
                    object oBookMark = strFieldName;
                    varDoc.Bookmarks.get_Item(ref oBookMark).Range.Text = customerValue;
                }
            //}
        }


        public void addFormField(string bkName, string defaultValue)
        {
            varDoc.FormFields.Add(varWord.Selection.Range, msWord.WdFieldType.wdFieldFormTextInput);
            varDoc.FormFields.get_Item("Text1").Name = bkName;
            varDoc.FormFields.get_Item(bkName).TextInput.Default = defaultValue;
            
        }


        public void addEmptyCheckBoxField()
        {
            varDoc.FormFields.Add(varWord.Selection.Range, msWord.WdFieldType.wdFieldFormCheckBox);
            //varDoc.FormFields.get_Item("Text1").Name = bkName;
            //varDoc.FormFields.get_Item(bkName).TextInput.Default = defaultValue;

        }


        private DataSet GetDataSet(string[] arrData)
        {
            string strInParamsNames = "customer_id,fromDate,toDate,pop";
            string strInParamsValues = arrData[1] + "," + arrData[2] + "," + arrData[3] + "," + arrData[4];
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return GiyusDB.clsDbGlobal.GetDsFromSPNoReturnValue("[giyus].[getRptStat]", arrInParamsNames, arrInParamsValues);
        }

        private void drawTable(DataTable dt)
        {
            varWord.Selection.TypeText(dt.Rows[0]["companyAndForm"].ToString().Split('$')[0]);
            varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
            varWord.Selection.TypeText(dt.Rows[0]["companyAndForm"].ToString().Split('$')[1]);
            varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
            varWord.Selection.TypeText(dt.Rows[0]["cnt2"].ToString());
            varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
            varWord.Selection.TypeText(dt.Rows[0]["per"].ToString());
            //goto next line
        }

        public void doReport(string[] arrData)//string emp_num, string strFileName, string resRole)
        {
            try
            {
                DataSet ds = GetDataSet(arrData);
                string strFileName = "";
                if (Convert.ToInt32(arrData[4]) == Program.enumPopulation.reguler.GetHashCode())
                {
                    strFileName = "rptStat";
                }
                else
                {
                    strFileName = "rptStatCheckUp";
                }
                object varFileName = Program.paramStrWordWorkDir + "\\" + strFileName + ".doc";

                // Create a reference to MS Word application
                varWord = new Microsoft.Office.Interop.Word.Application();

                // Open word
                varWord.Visible = blnWordVisible;
                if (blnWordVisible)
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsAll;
                }
                else
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;
                }
                // Creates a reference to a word document
                msg = "����� ������ ��� ������� ������: = " + varFileName;
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTable(msg,2, 3); //info,rptCompnay


                    objPass = Missing.Value;
                if (!File.Exists(varFileName.ToString()))
                {
                    msg = "�� ���� ���� ����";
                    Program.writeToScreenAndToLog(msg, true);
                    Program.writeToTasksLogTable(msg, 1, 3);
                    return;
                }
                varDoc =
                    varWord.Documents.Open(ref varFileName, ref objMissing, ref objFalse,
                       ref objMissing, ref objPass, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing,
                       ref objMissing, ref objMissing, ref objFalse, ref objMissing, ref objMissing, ref objMissing);

                // Activate the document
                varDoc.Activate();
                //for (int idx = 0; idx < varDoc.Bookmarks.Count; idx++)
                //{
                string strFileNoExt = Path.GetFileNameWithoutExtension(varFileName.ToString());

                Console.WriteLine("start building report file=" + varFileName + " - time=" + DateTime.Now);
                fillBookMark("date", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                fillBookMark("total", ds.Tables[0].Rows[0]["cnt"].ToString());
                fillBookMark("params", string.Format("���� ����: {0}            ������: {1}              �� �����: {2}", arrData[1], arrData[2], arrData[3]));

                varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "table");

                for (int idx = 1; idx < ds.Tables.Count; idx++)
                {
                    //do in all except first table (from table 1 to count)
                    //delete prev table bookmarks, and paste new table in new page.
                    if (ds.Tables[idx] != null && ds.Tables[idx].Rows.Count > 0)
                    {
                        drawTable(ds.Tables[idx]);
                    }
                    if (idx < ds.Tables.Count - 1)
                    {
                        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
                    }
                }
                Program.strNewFileName = strFileNoExt + "_" + 
                                string.Format("{0:dd_MM_yyyy hh_mm_ss}",DateTime.Now) + ".doc";

                strNewFile = Program.paramStrOutputDir + "\\reports\\" + strFileNoExt + "_" +
                                string.Format("{0:dd_MM_yyyy hh_mm_ss}", DateTime.Now) + ".doc";

                object format=msWord.WdSaveFormat.wdFormatDocument97;
                //varDoc.Protect(msWord.WdProtectionType.wdAllowOnlyFormFields, objMissing, "1", objMissing, objMissing);
                varDoc.SaveAs(ref strNewFile, ref format,
                    Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                varWord.Quit(ref objMissing, ref objMissing, ref objMissing);
                msg = "������ ������ !";
                Program.writeToScreenAndToLog(msg, false);
                Program.writeToTasksLogTable(msg, 2, 3);
            }
            catch (Exception varE)
            {
                    msg = "�����: " + varE.Message + "<br>";
                    Program.writeToScreenAndToLog(msg + varE.StackTrace, true);
                    Program.writeToTasksLogTable(msg, 1, 3);
                    varWord.Quit(ref objFalse, ref objMissing, ref objMissing);
                    msg = "������ ������ !";
                    Program.writeToScreenAndToLog(msg, false);
                    Program.writeToTasksLogTable(msg, 2, 3);
            }

        }

        //private void fillChild(string strType, int intChildCount,DataTable dt,Boolean blnAddFormField)
        //{
        //    varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart" + strType);
        //    if (intChildCount > 1)
        //    {
        //        varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter, 5, msWord.WdMovementType.wdExtend);
        //        varWord.Selection.InsertRowsBelow(intChildCount - 1); //add rows
        //        varWord.Selection.MoveLeft(msWord.WdUnits.wdCharacter, 1, msWord.WdMovementType.wdMove);
        //        varWord.Selection.MoveDown(msWord.WdUnits.wdLine, intChildCount - 1, msWord.WdMovementType.wdMove);
        //        varWord.Selection.MoveUp(msWord.WdUnits.wdLine, 1, msWord.WdMovementType.wdMove);
        //        varWord.Selection.MoveUp(msWord.WdUnits.wdLine, intChildCount - 1, msWord.WdMovementType.wdExtend);
        //        varWord.Selection.Cells.Merge();//merge right cell
        //        varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, "childStart" + strType);
        //        //move left untill first field of added child row
        //        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
        //    }
        //    else
        //    {
        //        //only one row - so skeep the rightest column with desc of type of children
        //        varWord.Selection.MoveRight(msWord.WdUnits.wdCell, 1, msWord.WdMovementType.wdMove);
        //    }
        //}



        //private Boolean Word_FindStr(string strToFind, Boolean blnForWard)
        //{

        //    //english - so if we used font barcod in english
        //    //it is returning to default font
        //    //resolve a bug !!!

        //    //			varWord.Keyboard (1033);		//eng

        //    //hebrew
        //    varWord.Keyboard(1037);
        //    object Miss = Missing.Value;
        //    varWord.Selection.Find.ClearFormatting();
        //    varWord.Selection.Find.Text = strToFind;
        //    //varWord.Selection.Find.Replacement.Text = NewStr;
        //    varWord.Selection.Find.Forward = blnForWard;
        //    varWord.Selection.Find.Wrap = msWord.WdFindWrap.wdFindContinue;
        //    varWord.Selection.Find.Format = true;
        //    varWord.Selection.Find.MatchCase = false;
        //    varWord.Selection.Find.MatchWholeWord = false;
        //    varWord.Selection.Find.MatchWildcards = false;
        //    varWord.Selection.Find.MatchSoundsLike = false;
        //    varWord.Selection.Find.MatchAllWordForms = false;
        //    varWord.Selection.Find.MatchKashida = false;
        //    varWord.Selection.Find.MatchDiacritics = false;
        //    varWord.Selection.Find.MatchAlefHamza = false;
        //    varWord.Selection.Find.MatchControl = false;

        //    //varWord.Selection.Find.Replacement.Font.Name = DefaultFontName_Hebrew;

        //    //varWord.Selection.Find.Replacement.Font.Size = DefaultFontSize;

        //    return varWord.Selection.Find.Execute
        //        (ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
        //        ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
        //        ref Miss, ref Miss, ref Miss, ref Miss, ref Miss);
        //}

    }
}