using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Diagnostics;

namespace GiyusDB
{
    public class clsQflowDbGlobal
    {
        private static SqlConnection myConnection = null;

        public static Boolean checkConn()
        {
            try
            {
                if (myConnection.State != System.Data.ConnectionState.Open)
                {
                    makeConn();
                }
                return true;
            }
            catch (Exception ex)
            {
                writeLog(ex.Message + "\n stack=" + ex.StackTrace, true);
                return false;
            }
        }

        public static Boolean makeConn()
        {
            try
            {
                string connStr = ConfigurationManager.ConnectionStrings
                        ["qflowConnectionString"].ConnectionString;
                myConnection = new SqlConnection(connStr);
                myConnection.Open();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on makeConn=\n" + ex.Message + "\n stack=" + ex.StackTrace , true);
                return false;
            }
        }



        //public static Boolean runTransactionSp_NoReturn(string[] spName, string[][] arrInParamsNames, string[][] arrInParamsValues)
        //{
        //    SqlTransaction tr = myConnection.BeginTransaction();
        //    try
        //    {
        //        if (!checkConn())
        //        {
        //            return false;
        //        }

        //        for (int spIndex = 0; spIndex < spName.Length; spIndex++)
        //        {
        //            SqlCommand cmd = new SqlCommand();
        //            cmd.Transaction = tr;
        //            int rowsAffected;
        //            cmd.CommandText = spName[spIndex];
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Connection = myConnection;
        //            for (int idx = 0; idx < arrInParamsNames[spIndex].Length; idx++)
        //            {
        //                if (arrInParamsValues[spIndex][idx].ToLower() == "null")
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[spIndex][idx], DBNull.Value));
        //                }
        //                else
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[spIndex][idx], arrInParamsValues[spIndex][idx]));
        //                }
        //            }
        //            rowsAffected = cmd.ExecuteNonQuery();
        //        }
        //        tr.Commit();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        tr.Rollback();
        //        writeLog("error on runSp_NoReturn=\n" +
        //                spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
        //        return false;
        //    }
        //}



        public static DataTable getParamsByTableNum(int addEmpty, int paramTbl)
        {
            string strInParamsNames = "addEmpty,paramTbl";
            string strInParamsValues = addEmpty + "," + paramTbl;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsDbGlobal.GetDtFromSPNoReturnValue("[giyus].[getParamsByTableNum]", arrInParamsNames, arrInParamsValues);
        }

        //public static string runSP(string spName, string[] arrInParamsNames, string[] arrInParamsValues, string[] arrOutParamsNames, string[] arrOutParamsValues)
        //{
        //    //ConfigurationManager.AppSettings["traceFolder"]
            
        //    SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
        //    myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
        //    for (int idx = 0; idx < arrInParamsNames.Length; idx++)
        //    {
        //        if (arrInParamsValues[idx].ToLower() == "null")
        //        {
        //            myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

        //        }
        //        else
        //        {
        //            myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
        //        }
        //    }

        //    for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
        //    {
        //        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrOutParamsNames[idx], null));
        //        myCommand.SelectCommand.Parameters[idx].Direction = ParameterDirection.Output;
        //    }


        //    object ret = myCommand.SelectCommand.ExecuteScalar();

        //    for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
        //    {
        //        arrOutParamsValues[idx] = myCommand.SelectCommand.Parameters[arrOutParamsNames[idx]].Value.ToString();
        //    }
        //    return ret.ToString();
        //}


        public static DataTable GetDtFromSP(string spName, string[] arrInParamsNames, string[] arrInParamsValues, string[] arrOutParamsNames, string[] arrOutParamsdbType,string[] arrOutParamsDbSize,string[] arrOutParamsValues)
        {
            try
            {
                if (!checkConn())
                {
                    return null;
                }
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }

                for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
                {
                    myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrOutParamsNames[idx], null));
                    myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Direction = ParameterDirection.Output;
                    if (arrOutParamsdbType[idx] == "int")
                    {
                        myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].DbType = DbType.Int32;
                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].DbType = DbType.String;
                    }
                    myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Size = Convert.ToInt32(arrOutParamsDbSize[idx]);
                }

                myCommand.Fill(dt);

                for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
                {
                    arrOutParamsValues[idx] = myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Value.ToString();
                }
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSP=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return null;
            }
        }



        public static DataTable GetDtFromSPNoReturnValue(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            try
            {
                if (!checkConn())
                {
                    return null;
                }
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                myCommand.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSPNoReturnValue using sp=\n" + 
                        spName + " - error=" + ex.Message + 
                        "\n stack=" + ex.StackTrace, true);
                return null;
            }
        }


        public static DataTable GetDtFromSPNoParamsNoReturnValue(string spName)
        {
            try
            {
                if (!checkConn())
                {
                    return null;
                }
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSPNoParamsNoReturnValue=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return null;
            }
        }

        public static Boolean runSp_NoReturn(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            try
            {
                if (!checkConn())
                {
                    return false;
                }
                //    DataSet objDs = new DataSet();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlCommand cmd = new SqlCommand();
                int rowsAffected;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = myConnection;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                rowsAffected = cmd.ExecuteNonQuery();
                //myConnection.Close();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return false;
            }
        }

        public static Boolean runSp_NonQuery_NoParams_NoReturn(string spName)
        {
            try
            {
                if (!checkConn())
                {
                    return false;
                }
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_NonQuery_NoParams_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return false;
            }
        }

        public static string runSp_ScalarQuery_NoParams_NoReturn(string spName)
        {
            try
            {
                if (!checkConn())
                {
                    return null;
                }
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
//                return cmd.ExecuteScalar().ToString();
                object myRet = cmd.ExecuteScalar();
                if (myRet != null)
                {
                    return myRet.ToString(); //ret.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_ScalarQuery_NoParams_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return "err: " + ex.Message;
            }
        }


        public static string runSp_ScalarQuery_NoReturn(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            try
            {
                if (!checkConn())
                {
                    return null;
                }
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                object myRet = cmd.ExecuteScalar();
                if (myRet != null)
                {
                    return myRet.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_ScalarQuery_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("err=" + ex.Message + " - stack = " + ex.StackTrace);
            }
        }

        #region helperFunction
        public static void writeLog(string msg, Boolean isErr)
        {
            string sSource = "GiyusDB";
            //string sLog = "GiyusDB";

            //if (!EventLog.SourceExists(sSource))
            //    EventLog.CreateEventSource(sSource, sLog);

            if (isErr)
            {
                EventLog.WriteEntry(sSource, " DB layer error found: \n\n" + msg
                                            , EventLogEntryType.Error,91);
            }
            else
            {
                EventLog.WriteEntry(sSource, " DB layer info msg: \n\n" + msg
                                            , EventLogEntryType.Information, 92);
            }
        }
        #endregion



        //public static Boolean InsertForConsole(string tableName, string strFields, string strValues)
        //{
        //    try
        //    {
        //        strFields = "[" + strFields.Replace(",", "],[") + "]";

        //        strValues = strValues.Replace(",,", ",null,");
        //        strValues = "'" + strValues.Replace(",","','") + "'";
                
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.Connection = myConnection;
        //        cmd.CommandText =   "insert into " + tableName +
        //                            "(" + strFields + ") values (" +
        //                            strValues + ")";
        //    writeLog("trying :" + cmd.CommandText, false);
        //        cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        writeLog("error on InsertForConsole=\n" + ex.Message + "\n stack=" + ex.StackTrace, true);
        //        return false;
        //    }
        //}


        //public static DataTable InsertForConsole(string tableName, string[] arrFields, string[] arrValues)
        //{
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandType = CommandType.Text;
        //        cmd.CommandText = "insert into " + tableName;
        //        cmd.Connection = myConnection;
        //        for (int idx = 0; idx < arrFields.Length; idx++)
        //        {
        //            strFiedls += arrFields[idx];
        //        }

        //        myCommand.ex

        //        for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
        //        {
        //            arrOutParamsValues[idx] = myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Value.ToString();
        //        }
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        writeLog("error on GetDtFromSP=\n" + ex.Message + "\n stack=" + ex.StackTrace, true);
        //        return null;
        //    }
        //}


    }
}
