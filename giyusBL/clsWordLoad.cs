using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using msWord = Microsoft.Office.Interop.Word;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.Web;
using System.IO;


namespace giyusBL
{
    public class clsWordLoad
    {
        Microsoft.Office.Interop.Word.Application varWord;
        Microsoft.Office.Interop.Word.Document varDoc;
        //Microsoft.Office.Interop.Word.Range varRange;
        //Microsoft.Office.Interop.Word.Bookmark varBookMark;

        object objFalse = false;
        object objTrue = true;
        object objMissing = Type.Missing;


        Boolean blnWordVisible = false; //Convert.ToBoolean(ConfigurationManager.AppSettings["wordVisible"]);
        Boolean blnWaitForKey = false; //Convert.ToBoolean(ConfigurationManager.AppSettings["waitForKey"]);
        object objPass = null;
        string msg = "";

        protected string cleanString(string myStr)
        {
            return myStr.Replace("\r\a", "").Trim().Replace("\r", " ").ToLower();
        }

        public void fillBookMark(string strFieldName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            string strBookMark = "";
            for (int idxCounter = 1; idxCounter <= 10; idxCounter++)
            {
                strBookMark = strFieldName + idxCounter.ToString();
                if (varDoc.Bookmarks.Exists(strBookMark))
                {
                    object oBookMark = strBookMark;
                    varDoc.Bookmarks.get_Item(ref oBookMark).Range.Text = customerValue;
                    //varDoc.FormFields.get_Item(1).Range.Select();
                    //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
                        //.MoveRight WdUnits.wdCharacter, Count:=1
                    //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
                    //varWord.Selection.Text = customerValue;

                }
            }
        }

        public void loadDoc(string strPathStart,string strWizardID)//string emp_num, string strFileName, string resRole)
        {
            try
            {
                object varFileName = HttpContext.Current.Application["paramStrWordWorkDir"].ToString() + 
                            "\\" + "haral_keren.doc";

                // Create a reference to MS Word application
                varWord = new Microsoft.Office.Interop.Word.Application();

                // Open word
                varWord.Visible = blnWordVisible;
                if (blnWordVisible)
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsAll;
                }
                else
                {
                    varWord.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;
                }
                // Creates a reference to a word document
                msg = "����� ������ �����: = " + varFileName;
                //Program.writeToScreenAndToLog(msg, false);
                //Program.writeToTasksLogTable(msg,2, 1);


                    objPass = Missing.Value;
                if (!File.Exists(varFileName.ToString()))
                {
                    msg = "�� ���� ����� ����� ���� ����";
                    ///Program.writeToScreenAndToLog(msg, true);
                    ///Program.writeToTasksLogTable(msg, 1, 1);
                    return;
                }
                varDoc =
                    varWord.Documents.Open(ref varFileName, ref objMissing, ref objTrue,
                       ref objMissing, ref objPass, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing,
                       ref objMissing, ref objMissing, ref objFalse, ref objMissing, ref objMissing, ref objMissing);

                // Activate the document
                varDoc.Activate();
                //for (int idx = 0; idx < varDoc.Bookmarks.Count; idx++)
                //{

                DataTable dt = (DataTable)HttpContext.Current.Session["dt"];

                    //Console.WriteLine("start bookmarks file=" + varFileName + " - time=" + DateTime.Now);
                    fillBookMark("address", dt.Rows[0]["customer_street"].ToString());
                    fillBookMark("cell", dt.Rows[0]["customer_cell"].ToString());
                    fillBookMark("dob", dt.Rows[0]["customer_birthDate"].ToString());
                    fillBookMark("id", dt.Rows[0]["customer_id"].ToString());
                    fillBookMark("name", dt.Rows[0]["customer_firstName"].ToString() +
                                    " " + dt.Rows[0]["customer_lastName"].ToString());
                    fillBookMark("nid", dt.Rows[0]["customer_nid"].ToString());
                    fillBookMark("phone", dt.Rows[0]["customer_phone"].ToString());
                    fillBookMark("sex", dt.Rows[0]["customer_sex"].ToString());
                    fillBookMark("today", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                    fillBookMark("zip", dt.Rows[0]["customer_zip"].ToString());
                    TypeTextBookMark("params", dt.Rows[0]["customer_id"].ToString() + "!$!" + strWizardID);

                //}

                    //object strNewFile = HttpContext.Current.Application["paramStrWordWorkDir"].ToString() + 
                    //        "\\docs\\" +  Path.GetFileNameWithoutExtension(varFileName.ToString()) +
                    //        "_" + dt.Rows[0]["customer_id"].ToString() + ".doc";


                    object strNewFile = HttpContext.Current.Server.MapPath("~/docs/files/" +
                        Path.GetFileNameWithoutExtension(varFileName.ToString()) +
                        "_" + dt.Rows[0]["customer_id"].ToString() + ".doc");


                    varDoc.Protect(msWord.WdProtectionType.wdAllowOnlyFormFields, objMissing, (object)"yaniv0383", objMissing, objMissing);

                object format=msWord.WdSaveFormat.wdFormatDocument97;
                varDoc.SaveAs(ref strNewFile, ref format,
                    Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                varWord.Quit(ref objMissing, ref objMissing, ref objMissing);
                msg = "������ ������ !";
                //Program.writeToScreenAndToLog(msg, false);
                //Program.writeToTasksLogTable(msg, 2, 1);
            }
            catch (Exception varE)
            {
                    msg = "�����: " + varE.Message + "<br>";
                    clsUtil.writeLog(msg + varE.StackTrace, true);
                    //Program.writeToScreenAndToLog(msg + varE.StackTrace, true);
                    //Program.writeToTasksLogTable(msg, 1, 1);
                    varWord.Quit(ref objFalse, ref objMissing, ref objMissing);
                    msg = "������ ������ !";
                    //Program.writeToScreenAndToLog(msg, false);
                    //Program.writeToTasksLogTable(msg, 2, 1);
            }

        }

        public void TypeTextBookMark(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            //varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, bkName);
            varWord.Selection.TypeText(customerValue);
            //varDoc.FormFields.get_Item(1).Range.Select();
            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }

            
            
        public void addBookMark(string bkName, string customerValue)//string emp_num, string strFileName, string resRole)
        {
            varDoc.Bookmarks.Add(bkName, objMissing);
            //varDoc.Bookmarks.get_Item(
            varWord.Selection.GoTo(msWord.WdGoToItem.wdGoToBookmark, objMissing, objMissing, bkName);
            varWord.Selection.TypeText("abc");
            //varDoc.FormFields.get_Item(1).Range.Select();
            //varWord.Selection.MoveRight(msWord.WdUnits.wdCharacter,1,objMissing);
            //.MoveRight WdUnits.wdCharacter, Count:=1
            //varDoc.GoTo(msWord.Paragraph. (object)msWord.WdMovementType.wdExtend //.Text = customerValue;
            //varWord.Selection.Text = customerValue;
        }
        private Boolean Word_FindStr(string strToFind, Boolean blnForWard)
        {

            //english - so if we used font barcod in english
            //it is returning to default font
            //resolve a bug !!!

            //			varWord.Keyboard (1033);		//eng

            //hebrew
            varWord.Keyboard(1037);
            object Miss = Missing.Value;
            varWord.Selection.Find.ClearFormatting();
            varWord.Selection.Find.Text = strToFind;
            //varWord.Selection.Find.Replacement.Text = NewStr;
            varWord.Selection.Find.Forward = blnForWard;
            varWord.Selection.Find.Wrap = msWord.WdFindWrap.wdFindContinue;
            varWord.Selection.Find.Format = true;
            varWord.Selection.Find.MatchCase = false;
            varWord.Selection.Find.MatchWholeWord = false;
            varWord.Selection.Find.MatchWildcards = false;
            varWord.Selection.Find.MatchSoundsLike = false;
            varWord.Selection.Find.MatchAllWordForms = false;
            varWord.Selection.Find.MatchKashida = false;
            varWord.Selection.Find.MatchDiacritics = false;
            varWord.Selection.Find.MatchAlefHamza = false;
            varWord.Selection.Find.MatchControl = false;

            //varWord.Selection.Find.Replacement.Font.Name = DefaultFontName_Hebrew;

            //varWord.Selection.Find.Replacement.Font.Size = DefaultFontSize;

            return varWord.Selection.Find.Execute
                (ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
                ref Miss, ref Miss, ref Miss, ref Miss, ref Miss,
                ref Miss, ref Miss, ref Miss, ref Miss, ref Miss);
        }

    }
}